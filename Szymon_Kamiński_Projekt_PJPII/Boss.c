#include"Header.h"

void Boss(){

	al_draw_filled_rectangle(0, 0, 1000, 40, al_map_rgb(255, 0, 0));

	al_draw_filled_rectangle(80, 40, 100, 80, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(320, 40, 340, 80, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(900, 40, 920, 80, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(660, 40, 680, 80, al_map_rgb(255, 0, 0));

	al_draw_filled_rectangle(410, 40, 590, 60, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(440, 60, 560, 80, al_map_rgb(255, 0, 0));

	al_draw_filled_triangle(175, 40, 205, 40, 205, 100, al_map_rgb(255, 0, 0));
	al_draw_filled_triangle(245, 40, 215, 40, 215, 100, al_map_rgb(255, 0, 0));

	al_draw_filled_triangle(825, 40, 795, 40, 795, 100, al_map_rgb(255, 0, 0));
	al_draw_filled_triangle(755, 40, 785, 40, 785, 100, al_map_rgb(255, 0, 0));

}

void StartT_L1_Bull(BossBullet bullets[], int count){
	for (int i = 0; i < count; i += 5){
		if ((!bullets[i].alive)&&(!bullets[i + 1].alive)&&(!bullets[i + 2].alive)&&(!bullets[i + 3].alive)&&(!bullets[i + 4].alive)) {
			if (rand() % 2000 == 0){
				bullets[i].alive = true;
				bullets[i].y = 80;
				bullets[i].x = 330;
				bullets[i].y_speed = -5;
				bullets[i].x_speed = 0;
				bullets[i].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i+1].alive = true;
				bullets[i + 1].y = 80;
				bullets[i + 1].x = 330;
				bullets[i + 1].y_speed = -5;
				bullets[i + 1].x_speed = 2;
				bullets[i+1].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i + 2].alive = true;
				bullets[i + 2].y = 80;
				bullets[i + 2].x = 330;
				bullets[i + 2].y_speed = -5;
				bullets[i + 2].x_speed = -2;
				bullets[i + 2].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i + 3].alive = true;
				bullets[i + 3].y = 80;
				bullets[i + 3].x = 330;
				bullets[i + 3].y_speed = -5;
				bullets[i + 3].x_speed = 4;
				bullets[i + 3].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i + 4].alive = true;
				bullets[i + 4].y = 80;
				bullets[i + 4].x = 330;
				bullets[i + 4].y_speed = -5;
				bullets[i + 4].x_speed = -4;
				bullets[i + 4].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

			}
		}
	}
}
void StartT_R1_Bull(BossBullet bullets[], int count){
	for (int i = 0; i < count; i += 5){
		if ((!bullets[i].alive) && (!bullets[i + 1].alive) && (!bullets[i + 2].alive) && (!bullets[i + 3].alive) && (!bullets[i + 4].alive)) {
			if (rand() % 2000 == 0){
				bullets[i].alive = true;
				bullets[i].y = 80;
				bullets[i].x = 670;
				bullets[i].y_speed = -5;
				bullets[i].x_speed = 0;
				bullets[i].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i+1].alive = true;
				bullets[i + 1].y = 80;
				bullets[i + 1].x = 670;
				bullets[i + 1].y_speed = -5;
				bullets[i + 1].x_speed = 2;
				bullets[i + 1].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i +2].alive = true;
				bullets[i + 2].y = 80;
				bullets[i + 2].x = 670;
				bullets[i + 2].y_speed = -5;
				bullets[i + 2].x_speed = -2;
				bullets[i + 2].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i + 3].alive = true;
				bullets[i + 3].y = 80;
				bullets[i + 3].x = 670;
				bullets[i + 3].y_speed = -5;
				bullets[i + 3].x_speed = 4;
				bullets[i + 3].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

				bullets[i + 4].alive = true;
				bullets[i + 4].y = 80;
				bullets[i + 4].x = 670;
				bullets[i + 4].y_speed = -5;
				bullets[i + 4].x_speed = -4;
				bullets[i + 4].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));

			}
		}
	}
}

void StartT_L2_Bull(BossBullet bullets[], int count, long long int c){
	for (int i = 0; i < count; i++){
		if (!bullets[i].alive){
			if (rand() % 2000 == 0){
				bullets[i].alive = true;
				bullets[i].y = 80;
				bullets[i].x = 90;
				bullets[i].y_speed = -0.5 - fabs(cos(c));
				bullets[i].x_speed = fabs(sin(c));
				bullets[i].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));
			}
		}
	}
}
void StartT_R2_Bull(BossBullet bullets[], int count, long long int c){
	for (int i = 0; i < count; i++){
		if (!bullets[i].alive){
			if (rand() % 2000 == 0){
				bullets[i].alive = true;
				bullets[i].y = 80;
				bullets[i].x = 910;
				bullets[i].y_speed = -0.5 - fabs(cos(c));
				bullets[i].x_speed = -fabs(sin(c));
				bullets[i].col = al_map_rgb((100 + (rand() % 255)), (100 + (rand() % 255)), (100 + (rand() % 255)));
			}
		}
	}
}

void InitBossBullet(BossBullet bullets[], int count){

	for (int i = 0; i < count; i++){
		bullets[i].alive = false;
		bullets[i].x_speed = 0;
		bullets[i].y_speed = 0;
	}

}

void DrawBossBullet(BossBullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			al_draw_filled_rectangle(bullets[i].x - 5, bullets[i].y + 5, bullets[i].x + 5, bullets[i].y - 5, bullets[i].col);
		}
	}

}

void UpdateBossBullet(BossBullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y -= bullets[i].y_speed;
			bullets[i].x += bullets[i].x_speed;
			if (bullets[i].y < -20 || bullets[i].x > 1050 || bullets[i].y > 750 || bullets[i].x < -20){
				bullets[i].alive = false;
			}
		}
	}

}

void P_BossBull_Coll(float x, float y, BossBullet bullets[], int count, short int *HP){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){

			if ((bullets[i].x + 5 > x - 3) && (bullets[i].x - 5 < x + 3) && (bullets[i].y + 5 > y - 5) && (bullets[i].y - 5 < y + 5)){
				(*HP)-=10;
				bullets[i].alive = false;
			}

		}
	}

}