
#include<allegro5\allegro.h>
#include<allegro5\allegro_native_dialog.h>
#include<allegro5\allegro_font.h>
#include<allegro5\allegro_ttf.h>
#include<allegro5\allegro_primitives.h>
#include<math.h>
#include<stdio.h>

typedef struct enemy{

	bool alive;
	float x;
	float y;
	float xcoef;

}Enemy;
typedef struct star{

	bool alive;
	float x;
	float y;
	float speed;

}Star;
typedef struct bullet{

	bool alive;
	float x;
	float y;

}Bullet;
typedef struct bossbullet{

	bool alive;
	float x;
	float y;
	float x_speed;
	float y_speed;
	ALLEGRO_COLOR col;

}BossBullet;
typedef struct rec
{
	char nick[9];
	long int score;

}RecScore;

Enemy Enemy_Vertical[48];
Enemy Enemy_Horizontal_Left[40];
Enemy Enemy_Horizontal_Right[40];
Enemy Enemy_Diagonal_Left[32];
Enemy Enemy_Diagonal_Right[32];

Star Stars[100];

Bullet Play_Bull[50];

Bullet Bullets_ED_U[200];
Bullet Bullets_ED_D[200];
Bullet Bullets_ED_R[200];
Bullet Bullets_ED_L[200];

Bullet Bullets_EV_UR[200];
Bullet Bullets_EV_DL[200];
Bullet Bullets_EV_DR[200];
Bullet Bullets_EV_UL[200];

Bullet Bullets_EH_D[200];
Bullet Bullets_EH_R[200];
Bullet Bullets_EH_L[200];

BossBullet TurL1[400];
BossBullet TurL2[400];
BossBullet TurR1[400];
BossBullet TurR2[400];



void player(float x, float y);

void Boss();

void InitEnemy(Enemy enemy[], int count);

void StartEnemyVert(Enemy enemy[], int count);
void StartEnemyHorR(Enemy enemy[], int count);
void StartEnemyHorL(Enemy enemy[], int count); 
void StartEnemyDiagR(Enemy enemy[], int count);
void StartEnemyDiagL(Enemy enemy[], int count);

void DrawEnemyVert(Enemy enemy[], int count);
void DrawEnemyHor(Enemy enemy[], int count);
void DrawEnemyDiag(Enemy enemy[], int count);

void UpdateEnemyVert(Enemy enemy[], int count);
void UpdateEnemyHorR(Enemy enemy[], int count);
void UpdateEnemyHorL(Enemy enemy[], int count);
void UpdateEnemyDiagR(Enemy enemy[], int count);
void UpdateEnemyDiagL(Enemy enemy[], int count);

void InitStar(Star stars[], int count);
void StartStar(Star stars[], int count);
void UpdateStar(Star stars[], int count);
void DrawStar(Star stars[], int count);


void InitBullet(Bullet bullets[], int count);
void StartBullet(Bullet bullets[], int count, Enemy enemy[], int z);
void DrawBulletVert(Bullet bullets[], int count);
void DrawBulletHor(Bullet bullets[], int count);
void DrawBulletDiag(Bullet bullets[], int count);

void UpdateBullet_U(Bullet bullets[], int count);
void UpdateBullet_D(Bullet bullets[], int count);
void UpdateBullet_R(Bullet bullets[], int count);
void UpdateBullet_L(Bullet bullets[], int count);
void UpdateBullet_UR(Bullet bullets[], int count);
void UpdateBullet_UL(Bullet bullets[], int count);
void UpdateBullet_DR(Bullet bullets[], int count);
void UpdateBullet_DL(Bullet bullets[], int count);

void P_BV_Coll(float x, float y, Bullet bullets[], int count, short int *HP);
void P_BH_Coll(float x, float y, Bullet bullets[], int count, short int *HP);
void P_BD_Coll(float x, float y, Bullet bullets[], int count, short int *HP);

void P_EV_Coll(float x, float y, Enemy enemies[], int count, short int *HP);
void P_EH_Coll(float x, float y, Enemy enemies[], int count, short int *HP);
void P_ED_Coll(float x, float y, Enemy enemies[], int count, short int *HP);

void StartPlayBull(Bullet bullets[], int count, float x, float y);
void UpdatePlayBullet(Bullet bullets[], int count);
void DrawPlayBullet(Bullet bullets[], int count);

void PB_EV_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn);
void PB_EH_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn);
void PB_ED_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn);

void InitBossBullet(BossBullet bullets[], int count);

void DrawBossBullet(BossBullet bullets[], int count);

void UpdateBossBullet(BossBullet bullets[], int count);

void StartT_L1_Bull(BossBullet bullets[], int count);
void StartT_L2_Bull(BossBullet bullets[], int count, long long int c);
void StartT_R1_Bull(BossBullet bullets[], int count);
void StartT_R2_Bull(BossBullet bullets[], int count, long long int c);

void P_BossBull_Coll(float x, float y, BossBullet bullets[], int count, short int *HP);