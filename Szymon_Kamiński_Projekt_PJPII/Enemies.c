#include"Header.h"

void InitEnemy(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		enemy[i].alive = false;
		enemy[i].xcoef = 1;
	}

}

void StartEnemyVert(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (!enemy[i].alive){
			if (rand() % 300 == 0){
				enemy[i].alive = true;
				enemy[i].y = 0;
				enemy[i].x = 10 + (rand() % 980);
			}
		}
	}
}
void StartEnemyHorR(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (!enemy[i].alive){
			if (rand() % 500 == 0){
				enemy[i].alive = true;
				enemy[i].x = 0;
				enemy[i].y = 10 + (rand() % 280);
			}
		}
	}
}
void StartEnemyHorL(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (!enemy[i].alive){
			if (rand() % 500 == 0){
				enemy[i].alive = true;
				enemy[i].x = 1000;
				enemy[i].y = 10 + (rand() % 280);
			}
		}
	}
}
void StartEnemyDiagR(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (!enemy[i].alive){
			if (rand() % 500 == 0){
				enemy[i].alive = true;
				enemy[i].x = 1000 +(rand() % 100);
				enemy[i].y =  -(rand() % 50);
				enemy[i].xcoef = (3 + (rand() % 20)) / 7;
			}
		}
	}
}
void StartEnemyDiagL(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (!enemy[i].alive){
			if (rand() % 500 == 0){
				enemy[i].alive = true;
				enemy[i].x = 0 -(rand() % 100);
				enemy[i].y = -(rand() % 50);
				enemy[i].xcoef = (3 + (rand() % 20)) / 7;
			}
		}
	}
}

void DrawEnemyVert(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			al_draw_filled_rectangle(enemy[i].x-8, enemy[i].y+15, enemy[i].x+8, enemy[i].y-15, al_map_rgb(255, 0, 0));
		}
	}
}
void DrawEnemyHor(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			al_draw_filled_rectangle(enemy[i].x - 20, enemy[i].y + 10, enemy[i].x + 20, enemy[i].y - 10, al_map_rgb(255, 0, 0));
		}
	}
}
void DrawEnemyDiag(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			al_draw_filled_rectangle(enemy[i].x - 12, enemy[i].y + 12, enemy[i].x + 12, enemy[i].y - 12, al_map_rgb(255, 0, 0));
		}
	}
}

void UpdateEnemyVert(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			enemy[i].y += 5;
			if (enemy[i].y > 750){
				enemy[i].alive = false;
			}
		}
	}
}
void UpdateEnemyHorR(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			enemy[i].x += 5;
			if (enemy[i].x > 1000){
				enemy[i].alive = false;
			}
		}
	}
}
void UpdateEnemyHorL(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			enemy[i].x -= 5;
			if (enemy[i].x < 0){
				enemy[i].alive = false;
			}
		}
	}
}
void UpdateEnemyDiagR(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			enemy[i].x -= 3 * (1 + enemy[i].xcoef);
			enemy[i].y += 3;
			if (enemy[i].x < -20 || enemy[i].y>750){
				enemy[i].alive = false;
			}
		}
	}
}
void UpdateEnemyDiagL(Enemy enemy[], int count){

	for (int i = 0; i < count; i++){
		if (enemy[i].alive){
			enemy[i].x += 3 * (1 + enemy[i].xcoef);
			enemy[i].y += 3;
			if (enemy[i].x > 1020 || enemy[i].y>750){
				enemy[i].alive = false;
			}
		}
	}
}

void PB_EV_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn){
	for (int j = 0; j < c; j++){
		for (int i = 0; i < count; i++){
			if (enemies[i].alive&&bullets[j].alive){
				if ((enemies[i].x + 8 > bullets[j].x - 2) && (enemies[i].x - 8 < bullets[j].x + 2) && (enemies[i].y + 15 > bullets[j].y - 4) && (enemies[i].y - 15 < bullets[j].y + 4)){
					(*Zn) += 100;
					enemies[i].alive = false;
					bullets[i].alive = false;
				}
			}
		}
	}
}
void PB_EH_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn){
	for (int j = 0; j < c; j++){
		for (int i = 0; i < count; i++){
			if (enemies[i].alive&&bullets[j].alive){
				if ((enemies[i].x + 20 > bullets[j].x - 2) && (enemies[i].x - 20 < bullets[j].x + 2) && (enemies[i].y + 10 > bullets[j].y - 4) && (enemies[i].y - 10 < bullets[j].y + 4)){
					(*Zn) += 100;
					enemies[i].alive = false;
					bullets[i].alive = false;
				}
			}
		}
	}
}
void PB_ED_Coll(Bullet bullets[], int c, Enemy enemies[], int count, long int *Zn){
	for (int j = 0; j < c; j++){
		for (int i = 0; i < count; i++){
			if (enemies[i].alive&&bullets[j].alive){
				if ((enemies[i].x + 12 > bullets[j].x - 2) && (enemies[i].x - 12 < bullets[j].x + 2) && (enemies[i].y + 12 > bullets[j].y - 4) && (enemies[i].y - 12 < bullets[j].y + 4)){
					(*Zn) += 100;
					enemies[i].alive = false;
					bullets[i].alive = false;
				}
			}
		}
	}
}