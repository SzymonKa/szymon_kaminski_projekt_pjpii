#include"Header.h"

void InitStar(Star stars[], int count){

	for (int i = 0; i < count; i++){
		stars[i].alive = false;
		stars[i].speed = 1;
	}

}

void StartStar(Star stars[], int count){

	for (int i = 0; i < count; i++){
		if (!stars[i].alive){
			if (rand() % 400 == 0){
				stars[i].alive = true;
				stars[i].y = -10;
				stars[i].x = 5 + (rand() % 990);
				stars[i].speed =1 + rand() % 3;
			}
		}
	}

}

void UpdateStar(Star stars[], int count){

	for (int i = 0; i < count; i++){
		if (stars[i].alive){
			stars[i].y += 3 * stars[i].speed;
			if (stars[i].y > 750){
				stars[i].alive = false;
			}
		}
	}

}

void DrawStar(Star stars[], int count){

	for (int i = 0; i < count; i++){
		if (stars[i].alive){
			al_draw_filled_rectangle(stars[i].x - 1, stars[i].y + 1, stars[i].x + 1, stars[i].y - 1, al_map_rgb(255, 255, 255));
		}
	}

}