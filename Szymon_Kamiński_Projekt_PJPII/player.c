#include"Header.h"

void player(float x, float y){


	al_draw_filled_triangle(x - 20, y + 5, x + 20, y + 5, x, y - 30, al_map_rgb(110, 110, 110));
	al_draw_filled_triangle(x - 20, y + 5, x + 20, y + 5, x, y + 15, al_map_rgb(110, 110, 110));
	al_draw_filled_rectangle(x-4, y-6, x+4, y+6, al_map_rgb(0, 255, 0));
	

}

void P_BD_Coll(float x, float y, Bullet bullets[], int count, short int *HP){

	for (int i = 0; i < count; i++){
		if(bullets[i].alive){
		
			if ((bullets[i].x + 5 > x - 3) && (bullets[i].x - 5 < x + 3) && (bullets[i].y + 5 > y - 5) && (bullets[i].y - 5 < y + 5)){
				(*HP)--;
				bullets[i].alive = false;
			}
			
		}
	}

}
void P_BH_Coll(float x, float y, Bullet bullets[], int count, short int *HP){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){

			if ((bullets[i].x + 5 > x - 3) && (bullets[i].x - 5 < x + 3) && (bullets[i].y + 3 > y - 5) && (bullets[i].y - 3 < y + 5)){
				(*HP)--;
				bullets[i].alive = false;
			}

		}
	}

}
void P_BV_Coll(float x, float y, Bullet bullets[], int count, short int *HP){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){

			if ((bullets[i].x + 3 > x - 3) && (bullets[i].x - 3 < x + 3) && (bullets[i].y + 5 > y - 5) && (bullets[i].y - 5 < y + 5)){
				(*HP)--;
				bullets[i].alive = false;
			}

		}
	}

}

void P_ED_Coll(float x, float y, Enemy enemies[], int count, short int *HP){
	for (int i = 0; i < count; i++){
		if (enemies[i].alive){
			if ((enemies[i].x + 12 > x - 3) && (enemies[i].x - 12 < x + 3) && (enemies[i].y + 12 > y - 5) && (enemies[i].y - 12 < y + 5)){
				(*HP)--;
				enemies[i].alive = false;
			}
		}
	}
}
void P_EH_Coll(float x, float y, Enemy enemies[], int count, short int *HP){
	for (int i = 0; i < count; i++){
		if (enemies[i].alive){
			if ((enemies[i].x + 20 > x - 3) && (enemies[i].x - 20 < x + 3) && (enemies[i].y + 10 > y - 5) && (enemies[i].y - 10 < y + 5)){
				(*HP)--;
				enemies[i].alive = false;
			}
		}
	}
}
void P_EV_Coll(float x, float y, Enemy enemies[], int count, short int *HP){
	for (int i = 0; i < count; i++){
		if (enemies[i].alive){
			if ((enemies[i].x + 8 > x - 3) && (enemies[i].x - 8 < x + 3) && (enemies[i].y + 15 > y - 5) && (enemies[i].y - 15 < y + 5)){
				(*HP)--;
				enemies[i].alive = false;
			}
		}
	}
}