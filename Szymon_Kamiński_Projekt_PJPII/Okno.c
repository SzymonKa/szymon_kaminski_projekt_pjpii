#include"Header.h"

short int dif = 2;


main(){


	RecScore S_E[10] = { 0 };
	RecScore S_N[10] = { 0 };
	RecScore S_H[10] = { 0 };
	RecScore S_I[10] = { 0 };
	RecScore A_E[10] = { 0 };
	RecScore A_N[10] = { 0 };
	RecScore A_H[10] = { 0 };
	RecScore A_I[10] = { 0 };

	FILE *f;

	int BossLives = 800;

	int vert_max_cout = 12;
	int hor_max_cout = 10;
	int diag_max_cout = 8;
	int bul_max_cout = 200;
	int Pbul_max_cout = 50;
	int boss_bull_max_count=100;

	long long int count = 0;
	long int punkty_czas=0;
	long int punkty_zniszczeni = 0;
	long int punkty_calk = 0;

	char znak='M';
	char znak2='M';

	short int poss=1;
	short int scooreposs = 1;
	short int nickposs = 0;
	short int g = 0;

	short int stage=1;

	short int T1 = 0;
	short int T2 = 0;
	short int T3 = 0;
	short int T4 = 0;

	short int Lives = 50;
	short int SLives = 50;

	al_init();
	float x = 500, y = 690;
	bool done = false;
	bool redraw = true;
	bool shoot = false;	
	ALLEGRO_TIMER *tiime = NULL;
	bool moveleft = false, moveright = false, moveup = false, movedown = false, z_key =false;
	float z_param = 1;

	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;

	RecScore temp1;
	RecScore temp2;

	temp2.nick[0] = 'a';
	temp2.nick[1] = 'a';
	temp2.nick[2] = 'a';
	temp2.nick[3] = 'a';
	temp2.nick[4] = 'a';
	temp2.nick[5] = 'a';
	temp2.nick[6] = 'a';
	temp2.nick[7] = 'a';
	temp2.score = 0;


	f = fopen("SE.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", S_E[i].nick, &S_E[i].score);

	}
	fclose(f);
	f = fopen("AE.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", A_E[i].nick, &A_E[i].score);

	}
	fclose(f);
	f = fopen("SN.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", S_N[i].nick, &S_N[i].score);

	}
	fclose(f);
	f = fopen("AN.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", A_N[i].nick, &A_N[i].score);

	}
	fclose(f);
	f = fopen("SH.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", S_H[i].nick, &S_H[i].score);

	}
	fclose(f);
	f = fopen("AH.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", A_H[i].nick, &A_H[i].score);

	}
	fclose(f);
	f = fopen("SI.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", S_I[i].nick, &S_I[i].score);

	}
	fclose(f);
	f = fopen("AI.txt", "r");
	for (int i = 0; i < 10; i++){

		fscanf(f, "%s %i\n", A_I[i].nick, &A_I[i].score);

	}
	fclose(f);

	display = al_create_display(1280, 720);
		
	InitEnemy(Enemy_Diagonal_Left, 32);
	InitEnemy(Enemy_Diagonal_Right, 32);
	InitEnemy(Enemy_Horizontal_Left, 40);
	InitEnemy(Enemy_Horizontal_Left, 40);
	InitEnemy(Enemy_Vertical, 48);

	InitStar(Stars,100);

	InitBullet(Bullets_ED_D, bul_max_cout);
	InitBullet(Bullets_ED_U, bul_max_cout);
	InitBullet(Bullets_ED_R, bul_max_cout);
	InitBullet(Bullets_ED_L, bul_max_cout);

	InitBullet(Bullets_EV_DL, bul_max_cout);
	InitBullet(Bullets_EV_DR, bul_max_cout);
	InitBullet(Bullets_EV_UR, bul_max_cout);
	InitBullet(Bullets_EV_UL, bul_max_cout);

	InitBullet(Bullets_EH_L, bul_max_cout);
	InitBullet(Bullets_EH_D, bul_max_cout);
	InitBullet(Bullets_EH_R, bul_max_cout);

	InitBossBullet(TurL1, boss_bull_max_count);
	InitBossBullet(TurL2, boss_bull_max_count);
	InitBossBullet(TurR1, boss_bull_max_count);
	InitBossBullet(TurR2, boss_bull_max_count);

	srand(time(NULL));
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_primitives_addon();
	al_install_keyboard();

	tiime = al_create_timer(1.0 / 60);
	event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(tiime));
	al_start_timer(tiime);	
	ALLEGRO_FONT *font24 = al_load_font("C:\\Windows\\Fonts\\Arial.ttf", 20, 0);
	while (!done){

		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		for (int qqq = 0; qqq < 8; qqq++){
			switch (znak)
			{
			case '1':{

				if (temp2.score >= A_E[0].score){

					temp1.nick[qqq] = A_E[0].nick[qqq];
					A_E[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[1].nick[qqq];
					A_E[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[2].nick[qqq];
					A_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[3].nick[qqq];
					A_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[4].nick[qqq];
					A_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= A_E[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[1].nick[qqq];
					A_E[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[2].nick[qqq];
					A_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[3].nick[qqq];
					A_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[4].nick[qqq];
					A_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[2].score){
					temp1.nick[qqq] = A_E[2].nick[qqq];
					A_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[3].nick[qqq];
					A_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[4].nick[qqq];
					A_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[3].nick[qqq];
					A_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[4].nick[qqq];
					A_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[4].score){
					temp1.nick[qqq] = A_E[4].nick[qqq];
					A_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[5].nick[qqq];
					A_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[6].score){
					temp1.nick[qqq] = A_E[6].nick[qqq];
					A_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[7].nick[qqq];
					A_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[8].score){
					temp1.nick[qqq] = A_E[8].nick[qqq];
					A_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_E[9].nick[qqq];
					A_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_E[9].score){
					A_E[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '2':{

				if (temp2.score >= A_N[0].score){

					temp1.nick[qqq] = A_N[0].nick[qqq];
					A_N[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[1].nick[qqq];
					A_N[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[2].nick[qqq];
					A_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[3].nick[qqq];
					A_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[4].nick[qqq];
					A_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= A_N[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[1].nick[qqq];
					A_N[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[2].nick[qqq];
					A_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[3].nick[qqq];
					A_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[4].nick[qqq];
					A_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[2].score){
					temp1.nick[qqq] = A_N[2].nick[qqq];
					A_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[3].nick[qqq];
					A_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[4].nick[qqq];
					A_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[3].nick[qqq];
					A_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[4].nick[qqq];
					A_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[4].score){
					temp1.nick[qqq] = A_N[4].nick[qqq];
					A_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[5].nick[qqq];
					A_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[6].score){
					temp1.nick[qqq] = A_N[6].nick[qqq];
					A_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[7].nick[qqq];
					A_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[8].score){
					temp1.nick[qqq] = A_N[8].nick[qqq];
					A_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_N[9].nick[qqq];
					A_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_N[9].score){
					A_N[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '3':{

				if (temp2.score >= A_H[0].score){

					temp1.nick[qqq] = A_H[0].nick[qqq];
					A_H[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[1].nick[qqq];
					A_H[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[2].nick[qqq];
					A_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[3].nick[qqq];
					A_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[4].nick[qqq];
					A_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= A_H[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[1].nick[qqq];
					A_H[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[2].nick[qqq];
					A_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[3].nick[qqq];
					A_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[4].nick[qqq];
					A_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[2].score){
					temp1.nick[qqq] = A_H[2].nick[qqq];
					A_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[3].nick[qqq];
					A_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[4].nick[qqq];
					A_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[3].nick[qqq];
					A_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[4].nick[qqq];
					A_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[4].score){
					temp1.nick[qqq] = A_H[4].nick[qqq];
					A_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[5].nick[qqq];
					A_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[6].score){
					temp1.nick[qqq] = A_H[6].nick[qqq];
					A_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[7].nick[qqq];
					A_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[8].score){
					temp1.nick[qqq] = A_H[8].nick[qqq];
					A_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_H[9].nick[qqq];
					A_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_H[9].score){
					A_H[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '4':{

				if (temp2.score >= A_I[0].score){

					temp1.nick[qqq] = A_I[0].nick[qqq];
					A_I[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[1].nick[qqq];
					A_I[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[2].nick[qqq];
					A_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[3].nick[qqq];
					A_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[4].nick[qqq];
					A_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= A_I[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[1].nick[qqq];
					A_I[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[2].nick[qqq];
					A_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[3].nick[qqq];
					A_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[4].nick[qqq];
					A_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[2].score){
					temp1.nick[qqq] = A_I[2].nick[qqq];
					A_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[3].nick[qqq];
					A_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[4].nick[qqq];
					A_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[3].nick[qqq];
					A_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[4].nick[qqq];
					A_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[4].score){
					temp1.nick[qqq] = A_I[4].nick[qqq];
					A_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[5].nick[qqq];
					A_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[6].score){
					temp1.nick[qqq] = A_I[6].nick[qqq];
					A_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[7].nick[qqq];
					A_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[8].score){
					temp1.nick[qqq] = A_I[8].nick[qqq];
					A_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = A_I[9].nick[qqq];
					A_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= A_I[9].score){
					A_I[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '5':{

				if (temp2.score >= S_E[0].score){

					temp1.nick[qqq] = S_E[0].nick[qqq];
					S_E[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[1].nick[qqq];
					S_E[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[2].nick[qqq];
					S_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[3].nick[qqq];
					S_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[4].nick[qqq];
					S_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= S_E[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[1].nick[qqq];
					S_E[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[2].nick[qqq];
					S_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[3].nick[qqq];
					S_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[4].nick[qqq];
					S_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[2].score){
					temp1.nick[qqq] = S_E[2].nick[qqq];
					S_E[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[3].nick[qqq];
					S_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[4].nick[qqq];
					S_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[3].nick[qqq];
					S_E[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[4].nick[qqq];
					S_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[4].score){
					temp1.nick[qqq] = S_E[4].nick[qqq];
					S_E[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[5].nick[qqq];
					S_E[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[6].score){
					temp1.nick[qqq] = S_E[6].nick[qqq];
					S_E[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[7].nick[qqq];
					S_E[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[8].score){
					temp1.nick[qqq] = S_E[8].nick[qqq];
					S_E[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_E[9].nick[qqq];
					S_E[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_E[9].score){
					S_E[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '6':{

				if (temp2.score >= S_N[0].score){

					temp1.nick[qqq] = S_N[0].nick[qqq];
					S_N[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[1].nick[qqq];
					S_N[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[2].nick[qqq];
					S_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[3].nick[qqq];
					S_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[4].nick[qqq];
					S_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= S_N[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[1].nick[qqq];
					S_N[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[2].nick[qqq];
					S_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[3].nick[qqq];
					S_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[4].nick[qqq];
					S_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[2].score){
					temp1.nick[qqq] = S_N[2].nick[qqq];
					S_N[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[3].nick[qqq];
					S_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[4].nick[qqq];
					S_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[3].nick[qqq];
					S_N[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[4].nick[qqq];
					S_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[4].score){
					temp1.nick[qqq] = S_N[4].nick[qqq];
					S_N[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[5].nick[qqq];
					S_N[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[6].score){
					temp1.nick[qqq] = S_N[6].nick[qqq];
					S_N[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[7].nick[qqq];
					S_N[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[8].score){
					temp1.nick[qqq] = S_N[8].nick[qqq];
					S_N[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_N[9].nick[qqq];
					S_N[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_N[9].score){
					S_N[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '7':{

				if (temp2.score >= S_H[0].score){

					temp1.nick[qqq] = S_H[0].nick[qqq];
					S_H[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[1].nick[qqq];
					S_H[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[2].nick[qqq];
					S_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[3].nick[qqq];
					S_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[4].nick[qqq];
					S_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= S_H[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[1].nick[qqq];
					S_H[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[2].nick[qqq];
					S_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[3].nick[qqq];
					S_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[4].nick[qqq];
					S_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[2].score){
					temp1.nick[qqq] = S_H[2].nick[qqq];
					S_H[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[3].nick[qqq];
					S_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[4].nick[qqq];
					S_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[3].nick[qqq];
					S_H[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[4].nick[qqq];
					S_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[4].score){
					temp1.nick[qqq] = S_H[4].nick[qqq];
					S_H[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[5].nick[qqq];
					S_H[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[6].score){
					temp1.nick[qqq] = S_H[6].nick[qqq];
					S_H[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[7].nick[qqq];
					S_H[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[8].score){
					temp1.nick[qqq] = S_H[8].nick[qqq];
					S_H[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_H[9].nick[qqq];
					S_H[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_H[9].score){
					S_H[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			case '8':{

				if (temp2.score >= S_I[0].score){

					temp1.nick[qqq] = S_I[0].nick[qqq];
					S_I[0].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[1].nick[qqq];
					S_I[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[2].nick[qqq];
					S_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[3].nick[qqq];
					S_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[4].nick[qqq];
					S_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];

				}
				else if (temp2.score >= S_I[1].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[1].nick[qqq];
					S_I[1].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[2].nick[qqq];
					S_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[3].nick[qqq];
					S_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[4].nick[qqq];
					S_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[2].score){
					temp1.nick[qqq] = S_I[2].nick[qqq];
					S_I[2].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[3].nick[qqq];
					S_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[4].nick[qqq];
					S_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[3].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[3].nick[qqq];
					S_I[3].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[4].nick[qqq];
					S_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[4].score){
					temp1.nick[qqq] = S_I[4].nick[qqq];
					S_I[4].nick[qqq] = temp2.nick[qqq];

					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[5].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[5].nick[qqq];
					S_I[5].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[6].score){
					temp1.nick[qqq] = S_I[6].nick[qqq];
					S_I[6].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[7].score){
					temp1.nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[7].nick[qqq];
					S_I[7].nick[qqq] = temp1.nick[qqq];
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[8].score){
					temp1.nick[qqq] = S_I[8].nick[qqq];
					S_I[8].nick[qqq] = temp2.nick[qqq];
					temp2.nick[qqq] = S_I[9].nick[qqq];
					S_I[9].nick[qqq] = temp1.nick[qqq];
				}
				else if (temp2.score >= S_I[9].score){
					S_I[9].nick[qqq] = temp2.nick[qqq];
				}
			}
			default:
				break;
			}
		}
		
			switch (znak)
			{
			case '1':{

				if (temp2.score >= A_E[0].score){

					temp1.score = A_E[0].score;
					A_E[0].score = temp2.score;
					temp2.score = A_E[1].score;
					A_E[1].score = temp1.score;
					temp1.score = A_E[2].score;
					A_E[2].score = temp2.score;
					temp2.score = A_E[3].score;
					A_E[3].score = temp1.score;
					temp1.score = A_E[4].score;
					A_E[4].score = temp2.score;

					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;

				}
				else if (temp2.score >= A_E[1].score){
					temp1.score = temp2.score;
					temp2.score = A_E[1].score;
					A_E[1].score = temp1.score;
					temp1.score = A_E[2].score;
					A_E[2].score = temp2.score;
					temp2.score = A_E[3].score;
					A_E[3].score = temp1.score;
					temp1.score = A_E[4].score;
					A_E[4].score = temp2.score;

					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[2].score){
					temp1.score = A_E[2].score;
					A_E[2].score = temp2.score;
					temp2.score = A_E[3].score;
					A_E[3].score = temp1.score;
					temp1.score = A_E[4].score;
					A_E[4].score = temp2.score;

					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[3].score){
					temp1.score = temp2.score;
					temp2.score = A_E[3].score;
					A_E[3].score = temp1.score;
					temp1.score = A_E[4].score;
					A_E[4].score = temp2.score;

					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[4].score){
					temp1.score = A_E[4].score;
					A_E[4].score = temp2.score;

					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[5].score){
					temp1.score = temp2.score;
					temp2.score = A_E[5].score;
					A_E[5].score = temp1.score;
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[6].score){
					temp1.score = A_E[6].score;
					A_E[6].score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[7].score){
					temp1.score = temp2.score;
					temp2.score = A_E[7].score;
					A_E[7].score = temp1.score;
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[8].score){
					temp1.score = A_E[8].score;
					A_E[8].score = temp2.score;
					temp2.score = A_E[9].score;
					A_E[9].score = temp1.score;
				}
				else if (temp2.score >= A_E[9].score){
					A_E[9].score = temp2.score;
				}
			}
			case '2':{

				if (temp2.score >= A_N[0].score){

					temp1.score = A_N[0].score;
					A_N[0].score = temp2.score;
					temp2.score = A_N[1].score;
					A_N[1].score = temp1.score;
					temp1.score = A_N[2].score;
					A_N[2].score = temp2.score;
					temp2.score = A_N[3].score;
					A_N[3].score = temp1.score;
					temp1.score = A_N[4].score;
					A_N[4].score = temp2.score;

					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;

				}
				else if (temp2.score >= A_N[1].score){
					temp1.score = temp2.score;
					temp2.score = A_N[1].score;
					A_N[1].score = temp1.score;
					temp1.score = A_N[2].score;
					A_N[2].score = temp2.score;
					temp2.score = A_N[3].score;
					A_N[3].score = temp1.score;
					temp1.score = A_N[4].score;
					A_N[4].score = temp2.score;

					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[2].score){
					temp1.score = A_N[2].score;
					A_N[2].score = temp2.score;
					temp2.score = A_N[3].score;
					A_N[3].score = temp1.score;
					temp1.score = A_N[4].score;
					A_N[4].score = temp2.score;

					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[3].score){
					temp1.score = temp2.score;
					temp2.score = A_N[3].score;
					A_N[3].score = temp1.score;
					temp1.score = A_N[4].score;
					A_N[4].score = temp2.score;

					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[4].score){
					temp1.score = A_N[4].score;
					A_N[4].score = temp2.score;

					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[5].score){
					temp1.score = temp2.score;
					temp2.score = A_N[5].score;
					A_N[5].score = temp1.score;
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[6].score){
					temp1.score = A_N[6].score;
					A_N[6].score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[7].score){
					temp1.score = temp2.score;
					temp2.score = A_N[7].score;
					A_N[7].score = temp1.score;
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[8].score){
					temp1.score = A_N[8].score;
					A_N[8].score = temp2.score;
					temp2.score = A_N[9].score;
					A_N[9].score = temp1.score;
				}
				else if (temp2.score >= A_N[9].score){
					A_N[9].score = temp2.score;
				}
			}
			case '3':{

				if (temp2.score >= A_H[0].score){

					temp1.score = A_H[0].score;
					A_H[0].score = temp2.score;
					temp2.score = A_H[1].score;
					A_H[1].score = temp1.score;
					temp1.score = A_H[2].score;
					A_H[2].score = temp2.score;
					temp2.score = A_H[3].score;
					A_H[3].score = temp1.score;
					temp1.score = A_H[4].score;
					A_H[4].score = temp2.score;

					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;

				}
				else if (temp2.score >= A_H[1].score){
					temp1.score = temp2.score;
					temp2.score = A_H[1].score;
					A_H[1].score = temp1.score;
					temp1.score = A_H[2].score;
					A_H[2].score = temp2.score;
					temp2.score = A_H[3].score;
					A_H[3].score = temp1.score;
					temp1.score = A_H[4].score;
					A_H[4].score = temp2.score;

					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[2].score){
					temp1.score = A_H[2].score;
					A_H[2].score = temp2.score;
					temp2.score = A_H[3].score;
					A_H[3].score = temp1.score;
					temp1.score = A_H[4].score;
					A_H[4].score = temp2.score;

					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[3].score){
					temp1.score = temp2.score;
					temp2.score = A_H[3].score;
					A_H[3].score = temp1.score;
					temp1.score = A_H[4].score;
					A_H[4].score = temp2.score;

					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[4].score){
					temp1.score = A_H[4].score;
					A_H[4].score = temp2.score;

					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[5].score){
					temp1.score = temp2.score;
					temp2.score = A_H[5].score;
					A_H[5].score = temp1.score;
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[6].score){
					temp1.score = A_H[6].score;
					A_H[6].score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[7].score){
					temp1.score = temp2.score;
					temp2.score = A_H[7].score;
					A_H[7].score = temp1.score;
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[8].score){
					temp1.score = A_H[8].score;
					A_H[8].score = temp2.score;
					temp2.score = A_H[9].score;
					A_H[9].score = temp1.score;
				}
				else if (temp2.score >= A_H[9].score){
					A_H[9].score = temp2.score;
				}
			}
			case '4':{

				if (temp2.score >= A_I[0].score){

					temp1.score = A_I[0].score;
					A_I[0].score = temp2.score;
					temp2.score = A_I[1].score;
					A_I[1].score = temp1.score;
					temp1.score = A_I[2].score;
					A_I[2].score = temp2.score;
					temp2.score = A_I[3].score;
					A_I[3].score = temp1.score;
					temp1.score = A_I[4].score;
					A_I[4].score = temp2.score;

					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;

				}
				else if (temp2.score >= A_I[1].score){
					temp1.score = temp2.score;
					temp2.score = A_I[1].score;
					A_I[1].score = temp1.score;
					temp1.score = A_I[2].score;
					A_I[2].score = temp2.score;
					temp2.score = A_I[3].score;
					A_I[3].score = temp1.score;
					temp1.score = A_I[4].score;
					A_I[4].score = temp2.score;

					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[2].score){
					temp1.score = A_I[2].score;
					A_I[2].score = temp2.score;
					temp2.score = A_I[3].score;
					A_I[3].score = temp1.score;
					temp1.score = A_I[4].score;
					A_I[4].score = temp2.score;

					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[3].score){
					temp1.score = temp2.score;
					temp2.score = A_I[3].score;
					A_I[3].score = temp1.score;
					temp1.score = A_I[4].score;
					A_I[4].score = temp2.score;

					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[4].score){
					temp1.score = A_I[4].score;
					A_I[4].score = temp2.score;

					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[5].score){
					temp1.score = temp2.score;
					temp2.score = A_I[5].score;
					A_I[5].score = temp1.score;
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[6].score){
					temp1.score = A_I[6].score;
					A_I[6].score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[7].score){
					temp1.score = temp2.score;
					temp2.score = A_I[7].score;
					A_I[7].score = temp1.score;
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[8].score){
					temp1.score = A_I[8].score;
					A_I[8].score = temp2.score;
					temp2.score = A_I[9].score;
					A_I[9].score = temp1.score;
				}
				else if (temp2.score >= A_I[9].score){
					A_I[9].score = temp2.score;
				}
			}			
			case '5':{

				if (temp2.score >= S_E[0].score){

					temp1.score = S_E[0].score;
					S_E[0].score = temp2.score;
					temp2.score = S_E[1].score;
					S_E[1].score = temp1.score;
					temp1.score = S_E[2].score;
					S_E[2].score = temp2.score;
					temp2.score = S_E[3].score;
					S_E[3].score = temp1.score;
					temp1.score = S_E[4].score;
					S_E[4].score = temp2.score;

					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;

				}
				else if (temp2.score >= S_E[1].score){
					temp1.score = temp2.score;
					temp2.score = S_E[1].score;
					S_E[1].score = temp1.score;
					temp1.score = S_E[2].score;
					S_E[2].score = temp2.score;
					temp2.score = S_E[3].score;
					S_E[3].score = temp1.score;
					temp1.score = S_E[4].score;
					S_E[4].score = temp2.score;

					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[2].score){
					temp1.score = S_E[2].score;
					S_E[2].score = temp2.score;
					temp2.score = S_E[3].score;
					S_E[3].score = temp1.score;
					temp1.score = S_E[4].score;
					S_E[4].score = temp2.score;

					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[3].score){
					temp1.score = temp2.score;
					temp2.score = S_E[3].score;
					S_E[3].score = temp1.score;
					temp1.score = S_E[4].score;
					S_E[4].score = temp2.score;

					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[4].score){
					temp1.score = S_E[4].score;
					S_E[4].score = temp2.score;

					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[5].score){
					temp1.score = temp2.score;
					temp2.score = S_E[5].score;
					S_E[5].score = temp1.score;
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[6].score){
					temp1.score = S_E[6].score;
					S_E[6].score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[7].score){
					temp1.score = temp2.score;
					temp2.score = S_E[7].score;
					S_E[7].score = temp1.score;
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[8].score){
					temp1.score = S_E[8].score;
					S_E[8].score = temp2.score;
					temp2.score = S_E[9].score;
					S_E[9].score = temp1.score;
				}
				else if (temp2.score >= S_E[9].score){
					S_E[9].score = temp2.score;
				}
			}
			case '6':{

				if (temp2.score >= S_N[0].score){

					temp1.score = S_N[0].score;
					S_N[0].score = temp2.score;
					temp2.score = S_N[1].score;
					S_N[1].score = temp1.score;
					temp1.score = S_N[2].score;
					S_N[2].score = temp2.score;
					temp2.score = S_N[3].score;
					S_N[3].score = temp1.score;
					temp1.score = S_N[4].score;
					S_N[4].score = temp2.score;

					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;

				}
				else if (temp2.score >= S_N[1].score){
					temp1.score = temp2.score;
					temp2.score = S_N[1].score;
					S_N[1].score = temp1.score;
					temp1.score = S_N[2].score;
					S_N[2].score = temp2.score;
					temp2.score = S_N[3].score;
					S_N[3].score = temp1.score;
					temp1.score = S_N[4].score;
					S_N[4].score = temp2.score;

					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[2].score){
					temp1.score = S_N[2].score;
					S_N[2].score = temp2.score;
					temp2.score = S_N[3].score;
					S_N[3].score = temp1.score;
					temp1.score = S_N[4].score;
					S_N[4].score = temp2.score;

					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[3].score){
					temp1.score = temp2.score;
					temp2.score = S_N[3].score;
					S_N[3].score = temp1.score;
					temp1.score = S_N[4].score;
					S_N[4].score = temp2.score;

					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[4].score){
					temp1.score = S_N[4].score;
					S_N[4].score = temp2.score;

					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[5].score){
					temp1.score = temp2.score;
					temp2.score = S_N[5].score;
					S_N[5].score = temp1.score;
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[6].score){
					temp1.score = S_N[6].score;
					S_N[6].score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[7].score){
					temp1.score = temp2.score;
					temp2.score = S_N[7].score;
					S_N[7].score = temp1.score;
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[8].score){
					temp1.score = S_N[8].score;
					S_N[8].score = temp2.score;
					temp2.score = S_N[9].score;
					S_N[9].score = temp1.score;
				}
				else if (temp2.score >= S_N[9].score){
					S_N[9].score = temp2.score;
				}
			}
			case '7':{

				if (temp2.score >= S_H[0].score){

					temp1.score = S_H[0].score;
					S_H[0].score = temp2.score;
					temp2.score = S_H[1].score;
					S_H[1].score = temp1.score;
					temp1.score = S_H[2].score;
					S_H[2].score = temp2.score;
					temp2.score = S_H[3].score;
					S_H[3].score = temp1.score;
					temp1.score = S_H[4].score;
					S_H[4].score = temp2.score;

					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;

				}
				else if (temp2.score >= S_H[1].score){
					temp1.score = temp2.score;
					temp2.score = S_H[1].score;
					S_H[1].score = temp1.score;
					temp1.score = S_H[2].score;
					S_H[2].score = temp2.score;
					temp2.score = S_H[3].score;
					S_H[3].score = temp1.score;
					temp1.score = S_H[4].score;
					S_H[4].score = temp2.score;

					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[2].score){
					temp1.score = S_H[2].score;
					S_H[2].score = temp2.score;
					temp2.score = S_H[3].score;
					S_H[3].score = temp1.score;
					temp1.score = S_H[4].score;
					S_H[4].score = temp2.score;

					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[3].score){
					temp1.score = temp2.score;
					temp2.score = S_H[3].score;
					S_H[3].score = temp1.score;
					temp1.score = S_H[4].score;
					S_H[4].score = temp2.score;

					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[4].score){
					temp1.score = S_H[4].score;
					S_H[4].score = temp2.score;

					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[5].score){
					temp1.score = temp2.score;
					temp2.score = S_H[5].score;
					S_H[5].score = temp1.score;
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[6].score){
					temp1.score = S_H[6].score;
					S_H[6].score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[7].score){
					temp1.score = temp2.score;
					temp2.score = S_H[7].score;
					S_H[7].score = temp1.score;
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[8].score){
					temp1.score = S_H[8].score;
					S_H[8].score = temp2.score;
					temp2.score = S_H[9].score;
					S_H[9].score = temp1.score;
				}
				else if (temp2.score >= S_H[9].score){
					S_H[9].score = temp2.score;
				}
			}
			case '8':{

				if (temp2.score >= S_I[0].score){

					temp1.score = S_I[0].score;
					S_I[0].score = temp2.score;
					temp2.score = S_I[1].score;
					S_I[1].score = temp1.score;
					temp1.score = S_I[2].score;
					S_I[2].score = temp2.score;
					temp2.score = S_I[3].score;
					S_I[3].score = temp1.score;
					temp1.score = S_I[4].score;
					S_I[4].score = temp2.score;

					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;

				}
				else if (temp2.score >= S_I[1].score){
					temp1.score = temp2.score;
					temp2.score = S_I[1].score;
					S_I[1].score = temp1.score;
					temp1.score = S_I[2].score;
					S_I[2].score = temp2.score;
					temp2.score = S_I[3].score;
					S_I[3].score = temp1.score;
					temp1.score = S_I[4].score;
					S_I[4].score = temp2.score;

					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[2].score){
					temp1.score = S_I[2].score;
					S_I[2].score = temp2.score;
					temp2.score = S_I[3].score;
					S_I[3].score = temp1.score;
					temp1.score = S_I[4].score;
					S_I[4].score = temp2.score;

					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[3].score){
					temp1.score = temp2.score;
					temp2.score = S_I[3].score;
					S_I[3].score = temp1.score;
					temp1.score = S_I[4].score;
					S_I[4].score = temp2.score;

					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[4].score){
					temp1.score = S_I[4].score;
					S_I[4].score = temp2.score;

					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[5].score){
					temp1.score = temp2.score;
					temp2.score = S_I[5].score;
					S_I[5].score = temp1.score;
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[6].score){
					temp1.score = S_I[6].score;
					S_I[6].score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[7].score){
					temp1.score = temp2.score;
					temp2.score = S_I[7].score;
					S_I[7].score = temp1.score;
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[8].score){
					temp1.score = S_I[8].score;
					S_I[8].score = temp2.score;
					temp2.score = S_I[9].score;
					S_I[9].score = temp1.score;
				}
				else if (temp2.score >= S_I[9].score){
					S_I[9].score = temp2.score;
				}
			}
			default:
				break;
			}
		
		

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN){

			if (znak == 'A' || znak == 'S'){
				switch (ev.keyboard.keycode){
				case ALLEGRO_KEY_UP:{
					moveup = true;
					break;
				}
				case ALLEGRO_KEY_DOWN:{
					movedown = true;
					break;
				}
				case ALLEGRO_KEY_LEFT:{
					moveleft = true;
					break;
				}
				case ALLEGRO_KEY_RIGHT:{
					moveright = true;
					break;
				}
				case ALLEGRO_KEY_Z:{
					z_key = true;
					break;
				}
				case ALLEGRO_KEY_SPACE:{
					shoot = true;
					break;
				}
				}
			}
		}

		if (ev.type == ALLEGRO_EVENT_KEY_UP){
			if (znak == 'A' || znak == 'S'){
										
					switch (ev.keyboard.keycode){
					case ALLEGRO_KEY_UP:{
						moveup = false;
						break;
					}
					case ALLEGRO_KEY_DOWN:{
						movedown = false;
						break;
					}
					case ALLEGRO_KEY_LEFT:{
						moveleft = false;
						break;
					}
					case ALLEGRO_KEY_RIGHT:{
						moveright = false;
						break;
					}
					case ALLEGRO_KEY_Z:{
						z_key = false;
						break;
					}
					case ALLEGRO_KEY_SPACE:{
						shoot = false;
						break;
					}
					case ALLEGRO_KEY_ESCAPE:{
						znak2 = znak;
						znak = 'P';
						poss = 1;
						break;

					}
					}
				}

			if (znak == 'M'){

					switch (ev.keyboard.keycode){
					case ALLEGRO_KEY_DOWN:{
						if (poss < 6){
							poss++;
						}
						else{
							poss = 1;
						}
						break;
					}
					case ALLEGRO_KEY_UP:{
						if (poss > 1){
							poss--;
						}
						else{
							poss = 6;
						}
						break;
					}
					case ALLEGRO_KEY_ENTER:{
						switch (poss){
						case 1:{
							znak = 'A';
							break;
						}
						case 2:{
							znak = 'S';
							break;
						}
						case 3:{
							znak = 'O';
							poss = 1;
							break;
						}
						case 4:{
							switch (dif)
							{
							case 1:{
								znak = '1';
								poss = 1;
								break;
							}
							case 2:{
								znak = '2';
								poss = 1;
								break;
							}
							case 3:{
								znak = '3';
								poss = 1;
								break;
							}
							case 4:{
								znak = '4';
								poss = 1;
								break;
							}
							default:
								break;
							}
							poss = 1;
							break;
						}
						case 5:{
							switch (dif)
							{
							case 1:{
								znak = '5';
								poss = 1;
								break;
							}
							case 2:{
								znak = '6';
								poss = 1;
								break;
							}
							case 3:{
								znak = '7';
								poss = 1;
								break;
							}
							case 4:{
								znak = '8';
								poss = 1;
								break;
							}
							default:
								break;
							}
							poss = 1;
							break;
						}
						case 6:{
							done = true;
							break;
						}
						default:
							break;
						}
						break;
					}
					case ALLEGRO_KEY_ESCAPE:{
						done = true;
						break;

					}
					}
				}

			if (znak == 'T'){
					switch (ev.keyboard.keycode){
					case ALLEGRO_KEY_ENTER:{
						znak = znak2;
						break;
					}
					case ALLEGRO_KEY_DOWN:{
						if (temp2.nick[nickposs] == 32){
							temp2.nick[nickposs] = 90;
						}
						else if (temp2.nick[nickposs] == 65){
							temp2.nick[nickposs] = 57;
						}
						else if (temp2.nick[nickposs] == 97){
							temp2.nick[nickposs] = 32;
						}
						else if (temp2.nick[nickposs] == 48){
							temp2.nick[nickposs] = 122;
						}
						else{
							temp2.nick[nickposs] --;
						}
						break;
					}
					case ALLEGRO_KEY_LEFT:{
						
							if (nickposs > 0){
								nickposs--;
							}
							else{
								nickposs = 7;
							}
						
						if (poss == 2){
							if (Lives > 25){
								Lives -= 25;
							}
						}
						break;
					}
					case ALLEGRO_KEY_RIGHT:{
						
						if (nickposs < 7){
							nickposs++;
							}
							else{
								nickposs = 0;
							}
						
						
						break;
					}
					case ALLEGRO_KEY_UP:{
						if (temp2.nick[nickposs] == 122){
							temp2.nick[nickposs] = 48;
						}
						else if (temp2.nick[nickposs] == 32) {
							temp2.nick[nickposs] = 97;
						}
						else if (temp2.nick[nickposs] == 57){
							temp2.nick[nickposs] = 65;
						}
						else if (temp2.nick[nickposs] == 90){
							temp2.nick[nickposs] = 32;
						}
						else{
							temp2.nick[nickposs] ++;
						}
						break;
					}

					}
				}
				
			if (znak == 'O'){
					
					switch (ev.keyboard.keycode){
					case ALLEGRO_KEY_DOWN:{
						if (poss<3){
							poss++;
						}
						else{
							poss = 1;
						}
						break;
					}
					case ALLEGRO_KEY_LEFT:{
						if (poss == 1){
							if (dif>1){
								dif--;
							}
							else{
								dif = 4;
							}
						}
						if (poss == 2){
							if (Lives > 25){
								Lives -= 25;
							}
						}
						break;
					}
					case ALLEGRO_KEY_RIGHT:{
						if (poss == 1){
							if (dif<4){
								dif++;
							}
							else{
								dif = 1;
							}
						}
						if (poss == 2){
							Lives += 25;
						}
						break;
					}
					case ALLEGRO_KEY_UP:{
						if (poss>1){
							poss--;
						}
						else{
							poss = 3;
						}
						break;
					}
					case ALLEGRO_KEY_ENTER:{
						if (poss == 3){

							znak = 'M';
							poss = 1;
							SLives = Lives;
						}
						break;
					}
					case ALLEGRO_KEY_ESCAPE:{
						znak = 'M';
						poss = 1;
						break;

					}
					}
				}

			if (znak == 'P'){
					/*if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE){
						znak = znak2;
						poss = 1;
					}*/
					switch (ev.keyboard.keycode){
					case ALLEGRO_KEY_DOWN:{
						if (poss<2){
							poss++;
						}
						else{
							poss = 1;
						}
						break;
					}
					case ALLEGRO_KEY_UP:{
						if (poss>1){
							poss--;
						}
						else{
							poss = 2;
						}
						break;
					}
					case ALLEGRO_KEY_ENTER:{
						if (poss == 1){

							znak = znak2;
							poss = 1;

						}
						if (poss == 2){

							stage = 1;
							znak = 'M';
							poss = 1;
							Lives = SLives;
							count = 0;
							punkty_czas = 0;
							punkty_zniszczeni = 0;
							punkty_calk = 0;

							InitEnemy(Enemy_Diagonal_Left, 4*diag_max_cout);
							InitEnemy(Enemy_Diagonal_Right, 4*diag_max_cout);
							InitEnemy(Enemy_Horizontal_Left, 4*hor_max_cout);
							InitEnemy(Enemy_Horizontal_Left, 4*hor_max_cout);
							InitEnemy(Enemy_Vertical, 4*vert_max_cout);

							InitBullet(Bullets_ED_D, bul_max_cout);
							InitBullet(Bullets_ED_U, bul_max_cout);
							InitBullet(Bullets_ED_R, bul_max_cout);
							InitBullet(Bullets_ED_L, bul_max_cout);

							InitBullet(Bullets_EV_DL, bul_max_cout);
							InitBullet(Bullets_EV_DR, bul_max_cout);
							InitBullet(Bullets_EV_UR, bul_max_cout);
							InitBullet(Bullets_EV_UL, bul_max_cout);

							InitBullet(Bullets_EH_L, bul_max_cout);
							InitBullet(Bullets_EH_D, bul_max_cout);
							InitBullet(Bullets_EH_R, bul_max_cout);

							InitBossBullet(TurL1, boss_bull_max_count);
							InitBossBullet(TurL2, boss_bull_max_count);
							InitBossBullet(TurR1, boss_bull_max_count);
							InitBossBullet(TurR2, boss_bull_max_count);

						}
						break;
					}
					}
				}

			if (znak >= '1'&&znak <= '8'){
				switch (ev.keyboard.keycode){
				case ALLEGRO_KEY_ESCAPE:{
					znak = 'M';
					poss = 1;
					break;

				}
				}
			}
		}

			
		if (znak == 'M'){

			switch (poss){
				case 1:{
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 700, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 800, 600, 0, "Exit");
					break;
				}
				case 2:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 700, 600, 0, "Exit");
					break;
				}
				case 3:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 600, 0, "Exit");
					break;
				}
				case 4:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 600, 0, "Exit");
					break;
				}
				case 5:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 700, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 600, 0, "Exit");
					break;
				}
				case 6:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 800, 100, 0, "Adventure");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 700, 200, 0, "Survival");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 300, 0, "Options");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 400, 0, "Adv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 400, 500, 0, "Surv. Scoreboard");
					al_draw_textf(font24, al_map_rgb(255, 0, 0), 300, 600, 0, "Exit");
					break;
				}
				default:
					break;
			}


			al_draw_textf(font24, al_map_rgb(255, 255, 255), 1000, 100, 0, "Movement: Arrow keys");
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 1000, 150, 0, "Slow Movement: Hold Z");
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 1000, 200, 0, "Shooting: Space");
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 1000, 250, 0, "Akcept: Enter");


			al_draw_textf(font24, al_map_rgb(255, 255, 255), 900, 400, 0, "< Shows Scoreboard for current difficulty");
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 900, 500, 0, "< Shows Scoreboard for current difficulty");

			
			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));

		}

		if (znak == 'T'){

				switch (nickposs)
				{
				case 0:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " |%c| %c %c %c %c %c %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 1:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c |%c| %c %c %c %c %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 2:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c |%c| %c %c %c %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 3:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c %c |%c| %c %c %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 4:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c %c %c |%c| %c %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 5:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c %c %c %c |%c| %c %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 6:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c %c %c %c %c |%c| %c", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				case 7:{
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 300, 0, " %c %c %c %c %c %c %c |%c|", temp2.nick[0], temp2.nick[1], temp2.nick[2], temp2.nick[3], temp2.nick[4], temp2.nick[5], temp2.nick[6], temp2.nick[7]);
					break;
				}
				default:
					break;
				}
				
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 500, 0, "Press ENTER to finish");
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 250, 0, "Left or Right : Select possition");
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 150, 0, "Up or Down : Select character");

				al_flip_display();

				al_clear_to_color(al_map_rgb(0, 0, 0));

			}

		if (znak == 'O'){

				

			switch (poss){
			case 1:{
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 400, 200, 0, "Dificulty");
				switch (dif){
					case 1:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 450, 250, 0, "Easy");
						break;
					}
					case 2:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 450, 250, 0, "Normal");
						break;
					}
					case 3:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 450, 250, 0, "Hard");
						break;
					}
					case 4:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 450, 250, 0, "Insane");
						break;
					}
					default:
						break;
				}
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 300, 0, "Lives");
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 350, 0, "%i",Lives);
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 400, 0, "Back");
				break;
			}
			case 2:{
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 200, 0, "Dificulty");
				switch (dif){
					case 1:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 250, 0, "Easy");
						break;
					}
					case 2:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 250, 0, "Normal");
						break;
					}
					case 3:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 250, 0, "Hard");
						break;
					}
					case 4:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 250, 0, "Insane");
						break;
					}
					default:
						break;
				}
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 400, 300, 0, "Lives");
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 450, 350, 0, "%i", Lives);
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 400, 0, "Back");
				break;
			}
			case 3:{
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 200, 0, "Dificulty");
				switch (dif){
					case 1:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 650, 250, 0, "Easy");
						break;
					}
					case 2:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 650, 250, 0, "Normal");
						break;
					}
					case 3:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 650, 250, 0, "Hard");
						break;
					}
					case 4:{
						al_draw_textf(font24, al_map_rgb(255, 255, 255), 650, 250, 0, "Insane");
						break;
					}
					default:
						break;
				}
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 500, 300, 0, "Lives");
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 350, 0, "%i", Lives);
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 400, 400, 0, "Back");
				break;
			}
			
			default:
				break;
			}

		

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));

		}

		if (znak == 'P'){
									
			switch (poss){
			case 1:{
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 400, 200, 0, "Resume");				
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 400, 0, "Back to Menu");
				break;
			}
			case 2:{
				al_draw_textf(font24, al_map_rgb(255, 255, 255), 600, 200, 0, "Resume");
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 400, 400, 0, "Back to Menu");
				break;
			}		

			default:
				break;
			}

			

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));

		}

		if (znak == '1'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", A_E[0].nick[0], A_E[0].nick[1], A_E[0].nick[2], A_E[0].nick[3], A_E[0].nick[4], A_E[0].nick[5], A_E[0].nick[6], A_E[0].nick[7], A_E[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", A_E[1].nick[0], A_E[1].nick[1], A_E[1].nick[2], A_E[1].nick[3], A_E[1].nick[4], A_E[1].nick[5], A_E[1].nick[6], A_E[1].nick[7],A_E[1].score);
			
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", A_E[2].nick[0], A_E[2].nick[1], A_E[2].nick[2], A_E[2].nick[3], A_E[2].nick[4], A_E[2].nick[5], A_E[2].nick[6], A_E[2].nick[7], A_E[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", A_E[3].nick[0], A_E[3].nick[1], A_E[3].nick[2], A_E[3].nick[3], A_E[3].nick[4], A_E[3].nick[5], A_E[3].nick[6], A_E[3].nick[7], A_E[3].score);
			
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", A_E[4].nick[0], A_E[4].nick[1], A_E[4].nick[2], A_E[4].nick[3], A_E[4].nick[4], A_E[4].nick[5], A_E[4].nick[6], A_E[4].nick[7], A_E[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", A_E[5].nick[0], A_E[5].nick[1], A_E[5].nick[2], A_E[5].nick[3], A_E[5].nick[4], A_E[5].nick[5], A_E[5].nick[6], A_E[5].nick[7], A_E[5].score);
			
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", A_E[6].nick[0], A_E[6].nick[1], A_E[6].nick[2], A_E[6].nick[3], A_E[6].nick[4], A_E[6].nick[5], A_E[6].nick[6], A_E[6].nick[7], A_E[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", A_E[7].nick[0], A_E[7].nick[1], A_E[7].nick[2], A_E[7].nick[3], A_E[7].nick[4], A_E[7].nick[5], A_E[7].nick[6], A_E[7].nick[7], A_E[7].score);
			
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", A_E[8].nick[0], A_E[8].nick[1], A_E[8].nick[2], A_E[8].nick[3], A_E[8].nick[4], A_E[8].nick[5], A_E[8].nick[6], A_E[8].nick[7], A_E[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", A_E[9].nick[0], A_E[9].nick[1], A_E[9].nick[2], A_E[9].nick[3], A_E[9].nick[4], A_E[9].nick[5], A_E[9].nick[6], A_E[9].nick[7], A_E[9].score);
	
			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}
			
		if (znak == '2'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", A_N[0].nick[0], A_N[0].nick[1], A_N[0].nick[2], A_N[0].nick[3], A_N[0].nick[4], A_N[0].nick[5], A_N[0].nick[6], A_N[0].nick[7], A_N[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", A_N[1].nick[0], A_N[1].nick[1], A_N[1].nick[2], A_N[1].nick[3], A_N[1].nick[4], A_N[1].nick[5], A_N[1].nick[6], A_N[1].nick[7], A_N[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", A_N[2].nick[0], A_N[2].nick[1], A_N[2].nick[2], A_N[2].nick[3], A_N[2].nick[4], A_N[2].nick[5], A_N[2].nick[6], A_N[2].nick[7], A_N[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", A_N[3].nick[0], A_N[3].nick[1], A_N[3].nick[2], A_N[3].nick[3], A_N[3].nick[4], A_N[3].nick[5], A_N[3].nick[6], A_N[3].nick[7], A_N[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", A_N[4].nick[0], A_N[4].nick[1], A_N[4].nick[2], A_N[4].nick[3], A_N[4].nick[4], A_N[4].nick[5], A_N[4].nick[6], A_N[4].nick[7], A_N[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", A_N[5].nick[0], A_N[5].nick[1], A_N[5].nick[2], A_N[5].nick[3], A_N[5].nick[4], A_N[5].nick[5], A_N[5].nick[6], A_N[5].nick[7], A_N[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", A_N[6].nick[0], A_N[6].nick[1], A_N[6].nick[2], A_N[6].nick[3], A_N[6].nick[4], A_N[6].nick[5], A_N[6].nick[6], A_N[6].nick[7], A_N[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", A_N[7].nick[0], A_N[7].nick[1], A_N[7].nick[2], A_N[7].nick[3], A_N[7].nick[4], A_N[7].nick[5], A_N[7].nick[6], A_N[7].nick[7], A_N[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", A_N[8].nick[0], A_N[8].nick[1], A_N[8].nick[2], A_N[8].nick[3], A_N[8].nick[4], A_N[8].nick[5], A_N[8].nick[6], A_N[8].nick[7], A_N[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", A_N[9].nick[0], A_N[9].nick[1], A_N[9].nick[2], A_N[9].nick[3], A_N[9].nick[4], A_N[9].nick[5], A_N[9].nick[6], A_N[9].nick[7], A_N[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '3'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", A_H[0].nick[0], A_H[0].nick[1], A_H[0].nick[2], A_H[0].nick[3], A_H[0].nick[4], A_H[0].nick[5], A_H[0].nick[6], A_H[0].nick[7], A_H[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", A_H[1].nick[0], A_H[1].nick[1], A_H[1].nick[2], A_H[1].nick[3], A_H[1].nick[4], A_H[1].nick[5], A_H[1].nick[6], A_H[1].nick[7], A_H[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", A_H[2].nick[0], A_H[2].nick[1], A_H[2].nick[2], A_H[2].nick[3], A_H[2].nick[4], A_H[2].nick[5], A_H[2].nick[6], A_H[2].nick[7], A_H[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", A_H[3].nick[0], A_H[3].nick[1], A_H[3].nick[2], A_H[3].nick[3], A_H[3].nick[4], A_H[3].nick[5], A_H[3].nick[6], A_H[3].nick[7], A_H[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", A_H[4].nick[0], A_H[4].nick[1], A_H[4].nick[2], A_H[4].nick[3], A_H[4].nick[4], A_H[4].nick[5], A_H[4].nick[6], A_H[4].nick[7], A_H[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", A_H[5].nick[0], A_H[5].nick[1], A_H[5].nick[2], A_H[5].nick[3], A_H[5].nick[4], A_H[5].nick[5], A_H[5].nick[6], A_H[5].nick[7], A_H[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", A_H[6].nick[0], A_H[6].nick[1], A_H[6].nick[2], A_H[6].nick[3], A_H[6].nick[4], A_H[6].nick[5], A_H[6].nick[6], A_H[6].nick[7], A_H[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", A_H[7].nick[0], A_H[7].nick[1], A_H[7].nick[2], A_H[7].nick[3], A_H[7].nick[4], A_H[7].nick[5], A_H[7].nick[6], A_H[7].nick[7], A_H[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", A_H[8].nick[0], A_H[8].nick[1], A_H[8].nick[2], A_H[8].nick[3], A_H[8].nick[4], A_H[8].nick[5], A_H[8].nick[6], A_H[8].nick[7], A_H[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", A_H[9].nick[0], A_H[9].nick[1], A_H[9].nick[2], A_H[9].nick[3], A_H[9].nick[4], A_H[9].nick[5], A_H[9].nick[6], A_H[9].nick[7], A_H[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '4'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", A_I[0].nick[0], A_I[0].nick[1], A_I[0].nick[2], A_I[0].nick[3], A_I[0].nick[4], A_I[0].nick[5], A_I[0].nick[6], A_I[0].nick[7], A_I[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", A_I[1].nick[0], A_I[1].nick[1], A_I[1].nick[2], A_I[1].nick[3], A_I[1].nick[4], A_I[1].nick[5], A_I[1].nick[6], A_I[1].nick[7], A_I[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", A_I[2].nick[0], A_I[2].nick[1], A_I[2].nick[2], A_I[2].nick[3], A_I[2].nick[4], A_I[2].nick[5], A_I[2].nick[6], A_I[2].nick[7], A_I[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", A_I[3].nick[0], A_I[3].nick[1], A_I[3].nick[2], A_I[3].nick[3], A_I[3].nick[4], A_I[3].nick[5], A_I[3].nick[6], A_I[3].nick[7], A_I[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", A_I[4].nick[0], A_I[4].nick[1], A_I[4].nick[2], A_I[4].nick[3], A_I[4].nick[4], A_I[4].nick[5], A_I[4].nick[6], A_I[4].nick[7], A_I[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", A_I[5].nick[0], A_I[5].nick[1], A_I[5].nick[2], A_I[5].nick[3], A_I[5].nick[4], A_I[5].nick[5], A_I[5].nick[6], A_I[5].nick[7], A_I[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", A_I[6].nick[0], A_I[6].nick[1], A_I[6].nick[2], A_I[6].nick[3], A_I[6].nick[4], A_I[6].nick[5], A_I[6].nick[6], A_I[6].nick[7], A_I[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", A_I[7].nick[0], A_I[7].nick[1], A_I[7].nick[2], A_I[7].nick[3], A_I[7].nick[4], A_I[7].nick[5], A_I[7].nick[6], A_I[7].nick[7], A_I[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", A_I[8].nick[0], A_I[8].nick[1], A_I[8].nick[2], A_I[8].nick[3], A_I[8].nick[4], A_I[8].nick[5], A_I[8].nick[6], A_I[8].nick[7], A_I[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", A_I[9].nick[0], A_I[9].nick[1], A_I[9].nick[2], A_I[9].nick[3], A_I[9].nick[4], A_I[9].nick[5], A_I[9].nick[6], A_I[9].nick[7], A_I[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '5'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", S_E[0].nick[0], S_E[0].nick[1], S_E[0].nick[2], S_E[0].nick[3], S_E[0].nick[4], S_E[0].nick[5], S_E[0].nick[6], S_E[0].nick[7], S_E[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", S_E[1].nick[0], S_E[1].nick[1], S_E[1].nick[2], S_E[1].nick[3], S_E[1].nick[4], S_E[1].nick[5], S_E[1].nick[6], S_E[1].nick[7], S_E[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", S_E[2].nick[0], S_E[2].nick[1], S_E[2].nick[2], S_E[2].nick[3], S_E[2].nick[4], S_E[2].nick[5], S_E[2].nick[6], S_E[2].nick[7], S_E[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", S_E[3].nick[0], S_E[3].nick[1], S_E[3].nick[2], S_E[3].nick[3], S_E[3].nick[4], S_E[3].nick[5], S_E[3].nick[6], S_E[3].nick[7], S_E[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", S_E[4].nick[0], S_E[4].nick[1], S_E[4].nick[2], S_E[4].nick[3], S_E[4].nick[4], S_E[4].nick[5], S_E[4].nick[6], S_E[4].nick[7], S_E[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", S_E[5].nick[0], S_E[5].nick[1], S_E[5].nick[2], S_E[5].nick[3], S_E[5].nick[4], S_E[5].nick[5], S_E[5].nick[6], S_E[5].nick[7], S_E[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", S_E[6].nick[0], S_E[6].nick[1], S_E[6].nick[2], S_E[6].nick[3], S_E[6].nick[4], S_E[6].nick[5], S_E[6].nick[6], S_E[6].nick[7], S_E[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", S_E[7].nick[0], S_E[7].nick[1], S_E[7].nick[2], S_E[7].nick[3], S_E[7].nick[4], S_E[7].nick[5], S_E[7].nick[6], S_E[7].nick[7], S_E[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", S_E[8].nick[0], S_E[8].nick[1], S_E[8].nick[2], S_E[8].nick[3], S_E[8].nick[4], S_E[8].nick[5], S_E[8].nick[6], S_E[8].nick[7], S_E[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", S_E[9].nick[0], S_E[9].nick[1], S_E[9].nick[2], S_E[9].nick[3], S_E[9].nick[4], S_E[9].nick[5], S_E[9].nick[6], S_E[9].nick[7], S_E[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '6'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", S_N[0].nick[0], S_N[0].nick[1], S_N[0].nick[2], S_N[0].nick[3], S_N[0].nick[4], S_N[0].nick[5], S_N[0].nick[6], S_N[0].nick[7], S_N[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", S_N[1].nick[0], S_N[1].nick[1], S_N[1].nick[2], S_N[1].nick[3], S_N[1].nick[4], S_N[1].nick[5], S_N[1].nick[6], S_N[1].nick[7], S_N[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", S_N[2].nick[0], S_N[2].nick[1], S_N[2].nick[2], S_N[2].nick[3], S_N[2].nick[4], S_N[2].nick[5], S_N[2].nick[6], S_N[2].nick[7], S_N[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", S_N[3].nick[0], S_N[3].nick[1], S_N[3].nick[2], S_N[3].nick[3], S_N[3].nick[4], S_N[3].nick[5], S_N[3].nick[6], S_N[3].nick[7], S_N[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", S_N[4].nick[0], S_N[4].nick[1], S_N[4].nick[2], S_N[4].nick[3], S_N[4].nick[4], S_N[4].nick[5], S_N[4].nick[6], S_N[4].nick[7], S_N[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", S_N[5].nick[0], S_N[5].nick[1], S_N[5].nick[2], S_N[5].nick[3], S_N[5].nick[4], S_N[5].nick[5], S_N[5].nick[6], S_N[5].nick[7], S_N[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", S_N[6].nick[0], S_N[6].nick[1], S_N[6].nick[2], S_N[6].nick[3], S_N[6].nick[4], S_N[6].nick[5], S_N[6].nick[6], S_N[6].nick[7], S_N[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", S_N[7].nick[0], S_N[7].nick[1], S_N[7].nick[2], S_N[7].nick[3], S_N[7].nick[4], S_N[7].nick[5], S_N[7].nick[6], S_N[7].nick[7], S_N[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", S_N[8].nick[0], S_N[8].nick[1], S_N[8].nick[2], S_N[8].nick[3], S_N[8].nick[4], S_N[8].nick[5], S_N[8].nick[6], S_N[8].nick[7], S_N[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", S_N[9].nick[0], S_N[9].nick[1], S_N[9].nick[2], S_N[9].nick[3], S_N[9].nick[4], S_N[9].nick[5], S_N[9].nick[6], S_N[9].nick[7], S_N[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '7'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", S_H[0].nick[0], S_H[0].nick[1], S_H[0].nick[2], S_H[0].nick[3], S_H[0].nick[4], S_H[0].nick[5], S_H[0].nick[6], S_H[0].nick[7], S_H[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", S_H[1].nick[0], S_H[1].nick[1], S_H[1].nick[2], S_H[1].nick[3], S_H[1].nick[4], S_H[1].nick[5], S_H[1].nick[6], S_H[1].nick[7], S_H[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", S_H[2].nick[0], S_H[2].nick[1], S_H[2].nick[2], S_H[2].nick[3], S_H[2].nick[4], S_H[2].nick[5], S_H[2].nick[6], S_H[2].nick[7], S_H[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", S_H[3].nick[0], S_H[3].nick[1], S_H[3].nick[2], S_H[3].nick[3], S_H[3].nick[4], S_H[3].nick[5], S_H[3].nick[6], S_H[3].nick[7], S_H[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", S_H[4].nick[0], S_H[4].nick[1], S_H[4].nick[2], S_H[4].nick[3], S_H[4].nick[4], S_H[4].nick[5], S_H[4].nick[6], S_H[4].nick[7], S_H[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", S_H[5].nick[0], S_H[5].nick[1], S_H[5].nick[2], S_H[5].nick[3], S_H[5].nick[4], S_H[5].nick[5], S_H[5].nick[6], S_H[5].nick[7], S_H[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", S_H[6].nick[0], S_H[6].nick[1], S_H[6].nick[2], S_H[6].nick[3], S_H[6].nick[4], S_H[6].nick[5], S_H[6].nick[6], S_H[6].nick[7], S_H[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", S_H[7].nick[0], S_H[7].nick[1], S_H[7].nick[2], S_H[7].nick[3], S_H[7].nick[4], S_H[7].nick[5], S_H[7].nick[6], S_H[7].nick[7], S_H[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", S_H[8].nick[0], S_H[8].nick[1], S_H[8].nick[2], S_H[8].nick[3], S_H[8].nick[4], S_H[8].nick[5], S_H[8].nick[6], S_H[8].nick[7], S_H[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", S_H[9].nick[0], S_H[9].nick[1], S_H[9].nick[2], S_H[9].nick[3], S_H[9].nick[4], S_H[9].nick[5], S_H[9].nick[6], S_H[9].nick[7], S_H[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == '8'){

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 50, 0, " %c %c %c %c %c %c %c %c     %i", S_I[0].nick[0], S_I[0].nick[1], S_I[0].nick[2], S_I[0].nick[3], S_I[0].nick[4], S_I[0].nick[5], S_I[0].nick[6], S_I[0].nick[7], S_I[0].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 120, 0, " %c %c %c %c %c %c %c %c     %i", S_I[1].nick[0], S_I[1].nick[1], S_I[1].nick[2], S_I[1].nick[3], S_I[1].nick[4], S_I[1].nick[5], S_I[1].nick[6], S_I[1].nick[7], S_I[1].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 190, 0, " %c %c %c %c %c %c %c %c     %i", S_I[2].nick[0], S_I[2].nick[1], S_I[2].nick[2], S_I[2].nick[3], S_I[2].nick[4], S_I[2].nick[5], S_I[2].nick[6], S_I[2].nick[7], S_I[2].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 260, 0, " %c %c %c %c %c %c %c %c     %i", S_I[3].nick[0], S_I[3].nick[1], S_I[3].nick[2], S_I[3].nick[3], S_I[3].nick[4], S_I[3].nick[5], S_I[3].nick[6], S_I[3].nick[7], S_I[3].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 330, 0, " %c %c %c %c %c %c %c %c     %i", S_I[4].nick[0], S_I[4].nick[1], S_I[4].nick[2], S_I[4].nick[3], S_I[4].nick[4], S_I[4].nick[5], S_I[4].nick[6], S_I[4].nick[7], S_I[4].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 400, 0, " %c %c %c %c %c %c %c %c     %i", S_I[5].nick[0], S_I[5].nick[1], S_I[5].nick[2], S_I[5].nick[3], S_I[5].nick[4], S_I[5].nick[5], S_I[5].nick[6], S_I[5].nick[7], S_I[5].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 470, 0, " %c %c %c %c %c %c %c %c     %i", S_I[6].nick[0], S_I[6].nick[1], S_I[6].nick[2], S_I[6].nick[3], S_I[6].nick[4], S_I[6].nick[5], S_I[6].nick[6], S_I[6].nick[7], S_I[6].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 540, 0, " %c %c %c %c %c %c %c %c     %i", S_I[7].nick[0], S_I[7].nick[1], S_I[7].nick[2], S_I[7].nick[3], S_I[7].nick[4], S_I[7].nick[5], S_I[7].nick[6], S_I[7].nick[7], S_I[7].score);

			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 610, 0, " %c %c %c %c %c %c %c %c     %i", S_I[8].nick[0], S_I[8].nick[1], S_I[8].nick[2], S_I[8].nick[3], S_I[8].nick[4], S_I[8].nick[5], S_I[8].nick[6], S_I[8].nick[7], S_I[8].score);
			al_draw_textf(font24, al_map_rgb(255, 255, 255), 550, 680, 0, " %c %c %c %c %c %c %c %c     %i", S_I[9].nick[0], S_I[9].nick[1], S_I[9].nick[2], S_I[9].nick[3], S_I[9].nick[4], S_I[9].nick[5], S_I[9].nick[6], S_I[9].nick[7], S_I[9].score);

			al_flip_display();

			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		if (znak == 'A' || znak == 'S'){

			if (Lives < 1){

				temp2.score = punkty_calk;
				nickposs = 0;
				znak2 = znak;

				if (znak == 'A'){
					switch (dif)
					{
					case 1:{
						if (punkty_calk > A_E[9].score){
							znak = 'T';
							znak2 = '1';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 2:{
						if (punkty_calk > A_N[9].score){
							znak = 'T';
							znak2 = '2';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 3:{
						if (punkty_calk > A_H[9].score){
							znak = 'T';
							znak2 = '3';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 4:{
						if (punkty_calk > A_I[9].score){
							znak = 'T';
							znak2 = '4';
						}
						else{
							znak = 'M';
						}
						break;
					}
					default:{
						
						break;
					}
					}
				}

				if (znak == 'S'){
					switch (dif)
					{
					case 1:{
						if (punkty_calk > S_E[9].score){
							znak = 'T';
							znak2 = '5';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 2:{
						if (punkty_calk > S_N[9].score){
							znak = 'T';
							znak2 = '6';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 3:{
						if (punkty_calk > S_H[9].score){
							znak = 'T';
							znak2 = '7';
						}
						else{
							znak = 'M';
						}
						break;
					}
					case 4:{
						if (punkty_calk > S_I[9].score){
							znak = 'T';
							znak2 = '8';
						}
						else{
							znak = 'M';
						}
						break;
					}
					default:{						
						break;
					}
					}
				}

				stage = 1;
				
				poss = 1;
				Lives = SLives;
				count = 0;
				punkty_czas = 0;
				punkty_zniszczeni = 0;
				punkty_calk = 0;
				x = 500;
				y = 690;

				InitEnemy(Enemy_Diagonal_Left, 4*diag_max_cout);
				InitEnemy(Enemy_Diagonal_Right, 4*diag_max_cout);
				InitEnemy(Enemy_Horizontal_Left, 4*hor_max_cout);
				InitEnemy(Enemy_Horizontal_Left, 4*hor_max_cout);
				InitEnemy(Enemy_Vertical, 4*vert_max_cout);

				InitBullet(Bullets_ED_D, bul_max_cout);
				InitBullet(Bullets_ED_U, bul_max_cout);
				InitBullet(Bullets_ED_R, bul_max_cout);
				InitBullet(Bullets_ED_L, bul_max_cout);

				InitBullet(Bullets_EV_DL, bul_max_cout);
				InitBullet(Bullets_EV_DR, bul_max_cout);
				InitBullet(Bullets_EV_UR, bul_max_cout);
				InitBullet(Bullets_EV_UL, bul_max_cout);

				InitBullet(Bullets_EH_L, bul_max_cout);
				InitBullet(Bullets_EH_D, bul_max_cout);
				InitBullet(Bullets_EH_R, bul_max_cout);

				InitBossBullet(TurL1, boss_bull_max_count);
				InitBossBullet(TurL2, boss_bull_max_count);
				InitBossBullet(TurR1, boss_bull_max_count);
				InitBossBullet(TurR2, boss_bull_max_count);
			}
		}
		if (ev.type == ALLEGRO_EVENT_TIMER){

				StartStar(Stars, 100);
				UpdateStar(Stars, 100);

				redraw = true;
				

				if (znak == 'A'){
					count++;

					if (z_key){
						z_param = 0.4;
					}
					else{
						z_param = 1;
					}
					if (movedown && y < 710){
						y += 6 * z_param;
					}
					if (moveup && y > 10){
						y -= 6 * z_param;
					}
					if (moveleft && x > 10){
						x -= 6 * z_param;
					}
					if (moveright && x < 970){
						x += 6 * z_param;
					}

					

					if ((count % 900 == 0) && (stage < 8)){
						punkty_czas += 1000;
						stage++;
					}

					punkty_calk = punkty_czas + punkty_zniszczeni;

					switch (stage){

					case 1:{
						StartEnemyDiagR(Enemy_Diagonal_Right, dif*diag_max_cout);

						if ((Enemy_Diagonal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
						}
						break;
					}
					case 2:{
						StartEnemyHorL(Enemy_Horizontal_Left, dif*hor_max_cout);
						StartEnemyDiagR(Enemy_Diagonal_Right, dif*diag_max_cout);

						if ((Enemy_Diagonal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
						}
						if ((Enemy_Horizontal_Left[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EH_D, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_R, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_L, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
						}
						break;
					}
					case 3:{
						StartEnemyVert(Enemy_Vertical, dif*vert_max_cout);

						if ((Enemy_Vertical[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EV_DL, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_DR, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_UL, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_UR, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
						}
						break;
					}
					case 4:{
						StartEnemyHorR(Enemy_Horizontal_Right, dif*hor_max_cout);
						StartEnemyDiagL(Enemy_Diagonal_Left, dif*diag_max_cout);

						if ((Enemy_Diagonal_Left[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
						}
						if ((Enemy_Horizontal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EH_D, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_R, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_L, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
						}
						break;
					}
					case 5:{
						StartEnemyDiagL(Enemy_Diagonal_Left, dif*diag_max_cout);
						StartEnemyDiagR(Enemy_Diagonal_Right, dif*diag_max_cout);

						if ((Enemy_Diagonal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
						}
						if ((Enemy_Diagonal_Left[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
						}
						break;
					}
					case 6:{
						StartEnemyHorL(Enemy_Horizontal_Left, dif*hor_max_cout);
						StartEnemyHorR(Enemy_Horizontal_Right, dif*hor_max_cout);

						if ((Enemy_Horizontal_Left[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EH_D, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_R, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_L, bul_max_cout, Enemy_Horizontal_Left, count % (dif*hor_max_cout));
						}
						if ((Enemy_Horizontal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EH_D, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_R, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
							StartBullet(Bullets_EH_L, bul_max_cout, Enemy_Horizontal_Right, count % (dif*hor_max_cout));
						}
						break;
					}
					case 7:{
						StartEnemyVert(Enemy_Vertical, dif*vert_max_cout);
						StartEnemyDiagL(Enemy_Diagonal_Left, dif*diag_max_cout);
						StartEnemyDiagR(Enemy_Diagonal_Right, dif*diag_max_cout);

						if ((Enemy_Diagonal_Right[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Right, count % (dif*diag_max_cout));
						}
						if ((Enemy_Diagonal_Left[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_ED_D, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_R, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_L, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
							StartBullet(Bullets_ED_U, bul_max_cout, Enemy_Diagonal_Left, count % (dif*diag_max_cout));
						}
						if ((Enemy_Vertical[count % diag_max_cout].alive) && (count % 2 == 0)){
							StartBullet(Bullets_EV_DL, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_DR, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_UL, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
							StartBullet(Bullets_EV_UR, bul_max_cout, Enemy_Vertical, count % (dif*vert_max_cout));
						}
						break;
					}
					case 8:{
						if (y < 200 || BossLives<1){
							Lives--;
						}

						if (count % 600 == 0) {

							T1 = rand() % 2;
							T2 = rand() % 2;
							T3 = rand() % 2;
							T4 = rand() % 2;
						}
						while (T1 == 1 && T2 == 1 && T3 == 1 & T4 == 1){
							T1 = rand() % 2;
							T2 = rand() % 2;
							T3 = rand() % 2;
							T4 = rand() % 2;
						}

						if (T1 == 0){
							StartT_L1_Bull(TurL1, dif * boss_bull_max_count);
						}
						if (T2 == 0){
							StartT_R1_Bull(TurR1, dif * boss_bull_max_count);
						}

						if (T3 == 0){
							StartT_L2_Bull(TurL2, dif * boss_bull_max_count, count);
						}
						if (T4 == 0){
							StartT_R2_Bull(TurR2, dif * boss_bull_max_count, count);
						}

						UpdateBossBullet(TurL1, dif * boss_bull_max_count);
						UpdateBossBullet(TurL2, dif * boss_bull_max_count);
						UpdateBossBullet(TurR1, dif * boss_bull_max_count);
						UpdateBossBullet(TurR2, dif * boss_bull_max_count);

						P_BossBull_Coll(x, y, TurL1, dif * boss_bull_max_count, &Lives);
						P_BossBull_Coll(x, y, TurR1, dif * boss_bull_max_count, &Lives);
						P_BossBull_Coll(x, y, TurL2, dif * boss_bull_max_count, &Lives);
						P_BossBull_Coll(x, y, TurR2, dif * boss_bull_max_count, &Lives);

						for (int i = 0; i < 50; i++){
							if (Play_Bull[i].alive){
								if (Play_Bull[i].y < 50){
									BossLives--;
									Play_Bull[i].alive = false;
								}
							}
						}

						break;
					}

					default:{
						break;
					}
					}

					if (shoot){

						StartPlayBull(Play_Bull, 50, x, y);

					}

					UpdatePlayBullet(Play_Bull, 50);

					UpdateEnemyVert(Enemy_Vertical, dif * vert_max_cout);
					UpdateEnemyDiagL(Enemy_Diagonal_Left, dif *diag_max_cout);
					UpdateEnemyDiagR(Enemy_Diagonal_Right, dif * diag_max_cout);
					UpdateEnemyHorL(Enemy_Horizontal_Left, dif * hor_max_cout);
					UpdateEnemyHorR(Enemy_Horizontal_Right, dif * hor_max_cout);

					UpdateBullet_D(Bullets_ED_D, bul_max_cout);
					UpdateBullet_R(Bullets_ED_R, bul_max_cout);
					UpdateBullet_U(Bullets_ED_U, bul_max_cout);
					UpdateBullet_L(Bullets_ED_L, bul_max_cout);

					UpdateBullet_D(Bullets_EH_D, bul_max_cout);
					UpdateBullet_DR(Bullets_EH_R, bul_max_cout);
					UpdateBullet_DL(Bullets_EH_L, bul_max_cout);

					UpdateBullet_DR(Bullets_EV_DR, bul_max_cout);
					UpdateBullet_UR(Bullets_EV_UR, bul_max_cout);
					UpdateBullet_UL(Bullets_EV_UL, bul_max_cout);
					UpdateBullet_DL(Bullets_EV_DL, bul_max_cout);

					P_BD_Coll(x, y, Bullets_EV_DR, bul_max_cout, &Lives);
					P_BD_Coll(x, y, Bullets_EV_UR, bul_max_cout, &Lives);
					P_BD_Coll(x, y, Bullets_EV_UL, bul_max_cout, &Lives);
					P_BD_Coll(x, y, Bullets_EV_DL, bul_max_cout, &Lives);

					P_BD_Coll(x, y, Bullets_EH_R, bul_max_cout, &Lives);
					P_BD_Coll(x, y, Bullets_EH_L, bul_max_cout, &Lives);

					P_BH_Coll(x, y, Bullets_ED_R, bul_max_cout, &Lives);
					P_BH_Coll(x, y, Bullets_ED_L, bul_max_cout, &Lives);

					P_BV_Coll(x, y, Bullets_ED_D, bul_max_cout, &Lives);
					P_BV_Coll(x, y, Bullets_ED_U, bul_max_cout, &Lives);

					P_BV_Coll(x, y, Bullets_EH_D, bul_max_cout, &Lives);

					P_EV_Coll(x, y, Enemy_Vertical, dif * vert_max_cout, &Lives);

					P_EH_Coll(x, y, Enemy_Horizontal_Right, dif * hor_max_cout, &Lives);
					P_EH_Coll(x, y, Enemy_Horizontal_Left,dif * hor_max_cout, &Lives);

					P_ED_Coll(x, y, Enemy_Diagonal_Left, dif * diag_max_cout, &Lives);
					P_ED_Coll(x, y, Enemy_Diagonal_Right, dif * diag_max_cout, &Lives);

					PB_EV_Coll(Play_Bull, Pbul_max_cout, Enemy_Vertical, dif * vert_max_cout, &punkty_zniszczeni);

					PB_EH_Coll(Play_Bull, Pbul_max_cout, Enemy_Horizontal_Right, dif * hor_max_cout, &punkty_zniszczeni);
					PB_EH_Coll(Play_Bull, Pbul_max_cout, Enemy_Horizontal_Left, dif * hor_max_cout, &punkty_zniszczeni);

					PB_ED_Coll(Play_Bull, Pbul_max_cout, Enemy_Diagonal_Left, dif * diag_max_cout, &punkty_zniszczeni);
					PB_ED_Coll(Play_Bull, Pbul_max_cout, Enemy_Diagonal_Right, dif * diag_max_cout, &punkty_zniszczeni);

				}

				if (znak == 'S'){
					count++;
					if (y < 200){
						Lives--;
					}
					
					if (z_key){
						z_param = 0.4;
					}
					else{
						z_param = 1;
					}
					if (movedown && y < 710){
						y += 6 * z_param;
					}
					if (moveup && y > 10){
						y -= 6 * z_param;
					}
					if (moveleft && x > 10){
						x -= 6 * z_param;
					}
					if (moveright && x < 970){
						x += 6 * z_param;
					}
										
					if (count % 60 == 0){
						punkty_czas += 10;
					}

					if (count % 600 == 0) {
						
						T1 = rand() % 2;
						T2 = rand() % 2;
						T3 = rand() % 2;
						T4 = rand() % 2;
					}
					while (T1 == 1 && T2 == 1 && T3 == 1 & T4 == 1){
						T1 = rand() % 2;
						T2 = rand() % 2;
						T3 = rand() % 2;
						T4 = rand() % 2;
					}

					if (T1 == 0){
						StartT_L1_Bull(TurL1, dif * boss_bull_max_count);
					}
					if (T2 == 0){
						StartT_R1_Bull(TurR1, dif * boss_bull_max_count);
					}

					if (T3 == 0){
						StartT_L2_Bull(TurL2, dif * boss_bull_max_count, count);
					}
					if (T4 == 0){
						StartT_R2_Bull(TurR2, dif * boss_bull_max_count, count);
					}

					UpdateBossBullet(TurL1, dif * boss_bull_max_count);
					UpdateBossBullet(TurL2, dif * boss_bull_max_count);
					UpdateBossBullet(TurR1, dif * boss_bull_max_count);
					UpdateBossBullet(TurR2, dif * boss_bull_max_count);

					P_BossBull_Coll(x, y, TurL1, dif * boss_bull_max_count, &Lives);
					P_BossBull_Coll(x, y, TurR1, dif * boss_bull_max_count, &Lives);
					P_BossBull_Coll(x, y, TurL2, dif * boss_bull_max_count, &Lives);
					P_BossBull_Coll(x, y, TurR2, dif * boss_bull_max_count, &Lives);


					punkty_calk = punkty_czas + punkty_zniszczeni;
				}
			}


		if (redraw&&al_is_event_queue_empty(event_queue)){

				DrawStar(Stars, 100);

				if (znak == 'A'){
					player(x, y);

					switch (stage){

					case 8:{
						Boss();

						DrawBossBullet(TurL1, dif * boss_bull_max_count);
						DrawBossBullet(TurL2, dif * boss_bull_max_count);
						DrawBossBullet(TurR1, dif * boss_bull_max_count);
						DrawBossBullet(TurR2, dif * boss_bull_max_count);

						al_draw_line(50, 25, 50+BossLives,25 , al_map_rgb(255, 255, 255), 3);

						break;
					}

					default:{
						DrawEnemyHor(Enemy_Horizontal_Left, hor_max_cout);
						DrawEnemyHor(Enemy_Horizontal_Right, hor_max_cout);
						DrawEnemyDiag(Enemy_Diagonal_Left, diag_max_cout);
						DrawEnemyDiag(Enemy_Diagonal_Right, diag_max_cout);
						DrawEnemyVert(Enemy_Vertical, vert_max_cout);

						DrawBulletHor(Bullets_ED_L, bul_max_cout);
						DrawBulletHor(Bullets_ED_R, bul_max_cout);
						DrawBulletVert(Bullets_ED_D, bul_max_cout);
						DrawBulletVert(Bullets_ED_U, bul_max_cout);

						DrawBulletVert(Bullets_EH_D, bul_max_cout);
						DrawBulletDiag(Bullets_EH_R, bul_max_cout);
						DrawBulletDiag(Bullets_EH_L, bul_max_cout);

						DrawBulletDiag(Bullets_EV_DR, bul_max_cout);
						DrawBulletDiag(Bullets_EV_UR, bul_max_cout);
						DrawBulletDiag(Bullets_EV_UL, bul_max_cout);
						DrawBulletDiag(Bullets_EV_DL, bul_max_cout);
						break;
					}
					}

					DrawPlayBullet(Play_Bull, 50);

					al_draw_line(1000, 0, 1000, 720, al_map_rgb(255, 255, 255), 3);

					al_draw_filled_rectangle(1000, 0, 1280, 720, al_map_rgb(0, 0, 0));



					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1015, 10, 0, "Stage: %i", stage);
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1015, 80, 0, "Points:");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1035, 105, 0, " %i", punkty_calk);
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1015, 200, 0, "Lives: %i", Lives);

					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1010, 400, 0, "Movement: Arrow keys");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1010, 450, 0, "Slow Movement: Hold Z");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1010, 500, 0, "Shooting: Space");

					al_flip_display();

					al_clear_to_color(al_map_rgb(0, 0, 0));

				}

				if (znak == 'S'){
					al_draw_filled_rectangle(0, 0, 1000, 200, al_map_rgb(50, 0, 0));
					Boss();

					

					player(x, y);

					DrawBossBullet(TurL1, dif * boss_bull_max_count);
					DrawBossBullet(TurL2, dif * boss_bull_max_count);
					DrawBossBullet(TurR1, dif * boss_bull_max_count);
					DrawBossBullet(TurR2, dif * boss_bull_max_count);
					
					al_draw_line(1000, 0, 1000, 720, al_map_rgb(255, 255, 255), 3);

					al_draw_filled_rectangle(1000, 0, 1280, 720, al_map_rgb(0, 0, 0));

					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1015, 80, 0, "Points:");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1035, 105, 0, " %i", punkty_calk);
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1015, 200, 0, "Lives: %i", Lives);

					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1010, 400, 0, "Movement: Arrow keys");
					al_draw_textf(font24, al_map_rgb(255, 255, 255), 1010, 450, 0, "Slow Movement: Hold Z");

					al_flip_display();

					al_clear_to_color(al_map_rgb(0, 0, 0));

				}
			}


	}

	al_destroy_event_queue(event_queue);
	al_destroy_display(display);


	f = fopen("SE.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", S_E[i].nick, S_E[i].score);
	}
	fclose(f);
	f = fopen("SN.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", S_N[i].nick, S_N[i].score);
	}
	fclose(f);
	f = fopen("SH.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", S_H[i].nick, S_H[i].score);
	}
	fclose(f);
	f = fopen("SI.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", S_I[i].nick, S_I[i].score);
	}
	fclose(f);
	f = fopen("AE.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", A_E[i].nick, A_E[i].score);
	}
	fclose(f);
	f = fopen("AN.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", A_N[i].nick, A_N[i].score);
	}
	fclose(f);
	f = fopen("AH.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", A_H[i].nick, A_H[i].score);
	}
	fclose(f);
	f = fopen("AI.txt", "w");
	for (int i = 0; i < 10; i++){
		fprintf(f, "%s %i\n", A_I[i].nick, A_I[i].score);
	}
	fclose(f);

	return 0;

}