#include"Header.h"

void InitBullet(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		bullets[i].alive = false;
	}

}
void StartBullet(Bullet bullets[], int count, Enemy enemy[], int z){

	for (int i = 0; i < count; i++){
		if (!bullets[i].alive&&enemy[z].alive){
			if (rand() % 125 == 0){
				bullets[i].alive = true;
				bullets[i].y = enemy[z].y;
				bullets[i].x = enemy[z].x;
			
			}
		}
	}

}
void DrawBulletVert(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			al_draw_filled_rectangle(bullets[i].x - 3, bullets[i].y + 5, bullets[i].x + 3, bullets[i].y - 5, al_map_rgb(255, 0, 255));
		}
	}

}
void DrawBulletHor(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			al_draw_filled_rectangle(bullets[i].x - 5, bullets[i].y + 3, bullets[i].x + 5, bullets[i].y - 3, al_map_rgb(255, 255, 0));
		}
	}

}
void DrawBulletDiag(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			al_draw_filled_rectangle(bullets[i].x - 5, bullets[i].y + 5, bullets[i].x + 5, bullets[i].y - 5, al_map_rgb(0, 255, 255));
		}
	}

}

void UpdateBullet_U(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y -= 3 ;
			if (bullets[i].y < -20){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_D(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y += 3;
			if (bullets[i].y > 750){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_R(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].x += 3;
			if (bullets[i].x > 1050){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_L(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].x -= 3;
			if (bullets[i].x < 0){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_UR(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y -= 8;
			bullets[i].x += 2;
			if (bullets[i].y < -20 || bullets[i].x > 1050){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_UL(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y -= 8;
			bullets[i].x -= 2;
			if (bullets[i].y < -20 || bullets[i].x < -20){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_DR(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y += 8;
			bullets[i].x += 2;
			if (bullets[i].y > 750 || bullets[i].x > 1050){
				bullets[i].alive = false;
			}
		}
	}

}
void UpdateBullet_DL(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y += 8;
			bullets[i].x -= 2;
			if (bullets[i].y > 750 || bullets[i].x < -20){
				bullets[i].alive = false;
			}
		}
	}

}


void StartPlayBull(Bullet bullets[], int count, float x, float y){

	for (int i = 0; i < count; i++){
		if (!bullets[i].alive){
			if (rand() % 300 == 0){
				bullets[i].alive = true;
				bullets[i].y = y;
				bullets[i].x = x;
			}
		}
	}
}
void UpdatePlayBullet(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			bullets[i].y -= 8;
			if (bullets[i].y < -20){
				bullets[i].alive = false;
			}
		}
	}

}
void DrawPlayBullet(Bullet bullets[], int count){

	for (int i = 0; i < count; i++){
		if (bullets[i].alive){
			al_draw_filled_rectangle(bullets[i].x - 3, bullets[i].y + 5, bullets[i].x + 3, bullets[i].y - 5, al_map_rgb(100, 255, 100));
		}
	}

}